<?php

/**
 * @file
 * Preprocess functions of vactory_cross_content module.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function vactory_cross_content_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

}

/**
 * Implements hook_preprocess_HOOK().
 */
function vactory_cross_content_preprocess_block(&$variables) {
  if ($variables['block']->delta === 'cross_content_block' && $variables['block']->module === 'vactory_cross_content') {
    // Custom js.
    drupal_add_js(drupal_get_path('module', 'vactory_cross_content') . '/js/vactory_cross_content.js');
  }

}
