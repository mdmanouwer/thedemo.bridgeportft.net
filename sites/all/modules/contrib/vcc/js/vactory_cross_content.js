/**
 * Created by Mounir on 10/08/16.
 */

(function ($) {
    $(document).ready(function () {

        var is_rtl = ($('html[dir="rtl"]').length) ? true : false;

        // Slick small slides slider
        $('.cross-content-wrapper .slider-grid').slick({
            dots: true,
            arrows: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            cssEase: 'cubic-bezier(0.585, -0.005, 0.635, 0.920)',
            useTransform: true,
            rtl: is_rtl,
            speed: 800,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            appendDots: $('.cross-content-wrapper'),
            nextArrow: '<button type="button" class="slick-arrow next ripple-effect"><i class="icon-chevron-right"></i></button>',
            prevArrow: '<button type="button" class="slick-arrow prev ripple-effect"><i class="icon-chevron-left"></i></button>'
        });

    });

})(jQuery);
