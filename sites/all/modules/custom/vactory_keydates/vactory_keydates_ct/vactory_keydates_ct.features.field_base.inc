<?php
/**
 * @file
 * vactory_keydates_ct.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function vactory_keydates_ct_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_keydates_dates'.
  $field_bases['field_keydates_dates'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_keydates_dates',
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'hide_initial_item' => 0,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  // Exported field_base: 'field_keydates_mode'.
  $field_bases['field_keydates_mode'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_keydates_mode',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'vertical' => 'Vertical',
        'horizontal' => 'Horizontal',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
