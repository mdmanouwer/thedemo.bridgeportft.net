/**
 * @file
 * Js file for timeline.
 */
(function ($) {
  $(document).ready(function () {
    var width = 0
    if ($(window).width() <= 768) {
      width = 150
    }
    $('#timeline.horisontal .timeline-item').each(function () {
      width = width + parseInt($(this).width())
    })
    $('#timeline.horisontal').append('<style> #timeline.horisontal:before{width:' + width + 'px;}</style>')
  })
})(jQuery)