<?php

/**
 * @file
 * Simple view of vactory_generator module.
 */

/**
 * Implements hook_views_api().
 */
function vactory_generator_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_views_api_alter().
 */
function vactory_generator_views_api_alter(&$apis) {
  // Define Vactory Generator Views default templates path.
  if (!empty($apis['vactory_generator']) && $apis['vactory_generator']['api'] == '3.0') {
    $apis['vactory_generator']['template path'] = drupal_get_path('module', 'vactory_generator') . '/templates';
  }
}

/**
 * Implements hook_views_default_views().
 */
function vactory_generator_views_default_views() {

  // Exported view goes here.

    $view = new view();
    $view->name = 'vactory_generator_view';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Vactory Generator View';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['css_class'] = 'listing';
    $handler->display->display_options['use_ajax'] = TRUE;
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['use_more_text'] = 'Plus';
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['query']['options']['distinct'] = TRUE;
    $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
    $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
    $handler->display->display_options['exposed_form']['options']['bef'] = array(
      'general' => array(
        'input_required' => 0,
        'text_input_required' => array(
          'text_input_required' => array(
            'value' => 'Select any filter and click on Apply to see results',
            'format' => 'filtered_html',
          ),
        ),
        'allow_secondary' => 0,
        'secondary_label' => 'Advanced options',
        'secondary_collapse_override' => '0',
      ),
    );
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '9';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['pager']['options']['id'] = '0';
    $handler->display->display_options['pager']['options']['quantity'] = '9';
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_listing_2_col';
    /* No results behavior: Global: Text area */
    $handler->display->display_options['empty']['area']['id'] = 'area';
    $handler->display->display_options['empty']['area']['table'] = 'views';
    $handler->display->display_options['empty']['area']['field'] = 'area';
    $handler->display->display_options['empty']['area']['empty'] = TRUE;
    $handler->display->display_options['empty']['area']['content'] = 'No results';
    $handler->display->display_options['empty']['area']['format'] = 'plain_text';
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = '';
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
    /* Filter criterion: Content: Language */
    $handler->display->display_options['filters']['language']['id'] = 'language';
    $handler->display->display_options['filters']['language']['table'] = 'node';
    $handler->display->display_options['filters']['language']['field'] = 'language';
    $handler->display->display_options['filters']['language']['value'] = array(
        '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    );
    $handler->display->display_options['filters']['language']['group'] = 1;

    /* Display: listing-1-col */
    $handler = $view->new_display('page', 'listing-1-col', 'listing_1_col');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Listing 1 col';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'listing listing-1-col';
    $handler->display->display_options['defaults']['use_ajax'] = FALSE;
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_listing_1_col';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['path'] = 'listing-1-col';

    /* Display: listing-2-col */
    $handler = $view->new_display('page', 'listing-2-col', 'listing_2_col');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Listing 2 col';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'listing listing-2-col';
    $handler->display->display_options['defaults']['use_ajax'] = FALSE;
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_listing_2_col';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['path'] = 'listing-2-col';

    /* Display: listing-3-col */
    $handler = $view->new_display('page', 'listing-3-col', 'listing_3_col');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Listing 3 col';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'listing listing-3-col';
    $handler->display->display_options['defaults']['use_ajax'] = FALSE;
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_listing_3_col';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['path'] = 'listing-3-col';

    /* Display: block-2-col */
    $handler = $view->new_display('block', 'block-2-col', 'block_2_col');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 2 col';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-2-col';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '6';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_2_col';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-3-col */
    $handler = $view->new_display('block', 'block-3-col', 'block_3_col');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 3 col';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-3-col';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '6';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_3_col';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-3s */
    $handler = $view->new_display('block', 'block-3s', 'block_3s');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 3s';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-3s';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '3';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_3s';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-5s */
    $handler = $view->new_display('block', 'block-5s', 'block_5s');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 5s';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-5s';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '5';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_5s';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-1b-3s */
    $handler = $view->new_display('block', 'block-1b-3s', 'block_1b_3s');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 1b-3s';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-1b-3s';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '4';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_1b_3s';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-1b-2s */
    $handler = $view->new_display('block', 'block-1b-2s', 'block_1b_2s');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block 1b-2s';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-1b-2s';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '3';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_1b_2s';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-slider-small */
    $handler = $view->new_display('block', 'block-slider-small', 'block_slider_small');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block slider-small';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-slider-small';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '6';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_slider_small';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-slider-large */
    $handler = $view->new_display('block', 'block-slider-large', 'block_slider_large');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block slider-large';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-slider-large';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '6';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_slider_large';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';

    /* Display: block-masonry */
    $handler = $view->new_display('block', 'block-masonry', 'block_masonry');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = 'Block masonry';
    $handler->display->display_options['defaults']['css_class'] = FALSE;
    $handler->display->display_options['css_class'] = 'block-masonry';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '6';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entity';
    $handler->display->display_options['row_options']['view_mode'] = 'vactory_gen_block_masonry';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Has taxonomy term ID */
    $handler->display->display_options['arguments']['tid']['id'] = 'tid';
    $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
    $handler->display->display_options['arguments']['tid']['field'] = 'tid';
    $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['tid']['default_argument_skip_url'] = TRUE;
    $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
    $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'empty';
    $translatables['vactory_generator_view'] = array(
      t('Master'),
      t('Plus'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Select any filter and click on Apply to see results'),
      t('Advanced options'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('No results'),
      t('listing-1-col'),
      t('Listing 1 col'),
      t('listing-2-col'),
      t('Listing 2 col'),
      t('listing-3-col'),
      t('Listing 3 col'),
      t('block-2-col'),
      t('Block 2 col'),
      t('block-3-col'),
      t('Block 3 col'),
      t('block-3s'),
      t('Block 3s'),
      t('All'),
      t('block-5s'),
      t('Block 5s'),
      t('block-1b-3s'),
      t('Block 1b-3s'),
      t('block-1b-2s'),
      t('Block 1b-2s'),
      t('block-slider-small'),
      t('Block slider-small'),
      t('block-slider-large'),
      t('Block slider-large'),
      t('block-masonry'),
      t('Block masonry'),
    );

  $views[$view->name] = $view;

  // Return views.
  return $views;
}

/**
 * Implements hook_form_views_exposed_form_alter().
 */
function vactory_generator_form_views_exposed_form_alter(&$form, &$form_state) {
  if (strpos($form_state['view']->name, 'vactory_gen_') === 0) {
    foreach ($form as $item => $value) {
      if (strpos($item, 'date_filter_m') === 0) {
        $form[$item]['value']['#date_format'] = 'm';
        $form['#validate'][] = 'vactory_generator_views_exposed_form_validate';
      }
      if (strpos($item, 'date_filter') === 0 &&
        $form[$item]['value']['#type'] == 'date_popup' &&
        $form[$item]['value']['#date_format'] == 'm/Y' &&
        strpos($item, 'date_filter_m') === false &&
        strpos($item, 'date_filter_y') === false) {
//        $form[$item]['value']['#date_label_position'] = '';
//        dsm($form[$item]);
        drupal_add_js(drupal_get_path('module', 'vactory_generator') . '/js/vactory_generator.js');
      }
    }
  }
}

/**
 * Implements hook_views_exposed_form_validate().
 */
function vactory_generator_views_exposed_form_validate(&$form, &$form_state) {
  $handlers = &$form_state['view']->filter;
  foreach ($handlers as $key => $handler) {
    if (strpos($handler->view->name, 'vactory_gen_') === 0 && isset($handler->options['granularity']) && $handler->options['granularity'] == 'month') {
      $handler->format = 'm';
    }
  }
}

/**
 * implements hook_element_info_alter()
 *
 */
function vactory_generator_element_info_alter(&$type) {
  if (isset($type['date_popup'])) {
    $type['date_popup']['#process'][] = 'vactory_generator_date_popup_process_alter';
  }
}

/**
 * function to remove the description from date_popup
 *
 */
function vactory_generator_date_popup_process_alter(&$element, &$form_state, $context) {
  unset($element['date']['#description']);
  $element['date']['#attributes']['placeholder'] = t('Filter by date');
  return $element;
}