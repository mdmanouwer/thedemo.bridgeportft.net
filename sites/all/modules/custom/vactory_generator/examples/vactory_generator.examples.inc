<?php

/**
 * @file
 * Example functions of vactory_generator module.
 */


/**
 * Implements hook_theme_registry_alter().
 */
function MY_MODULE_theme_registry_alter(&$theme_registry)
{
  // Defined path to the current module.
  $module_path = drupal_get_path('module', 'vactory_news_') . "/templates/node";
  // Find all .tpl.php files in this module's folder recursively.
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);
  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
      // Alter the theme path and template elements.
      $theme_registry[$key]['theme path'] = $module_path;
      $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
      $theme_registry[$key]['type'] = 'module';
    }
  }
}


/**
 * Implements hook_preprocess_node().
 *
 * Here is an example of loading js and libraries & other good things.
 */
function MY_MODULE_preprocess_node(&$variables) {

  if ($variables['type'] == 'MY_CONTENT_TYPE_NAME') {

    $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'].'__' . $variables['view_mode'];

    // Load custom js.
    drupal_add_js(drupal_get_path('module', 'vactory_generator') . '/js/vactory_generator.js');

    // Load Masonry library.
    if ($variables['view_mode'] == 'vactory_gen_block_masonry') {
      if (($library = libraries_detect('masonry')) && !empty($library['installed'])) {
        // Load Masonry assets.
        libraries_load('masonry');
        $path = libraries_get_path('masonry');
        drupal_add_js($path . '/masonry/dist/masonry.pkgd.min.js');
      }
      else {
        $error_message = $library['error message'];
        drupal_set_message($error_message, 'error');
      }
    }

    // Load Slick library.
    if ($variables['view_mode'] == 'vactory_gen_block_slider_large') {
      if (($library = libraries_detect('slick')) && !empty($library['installed'])) {
        // Load Masonry assets.
        libraries_load('slick');
        $slick_path = libraries_get_path('slick');
        drupal_add_css($slick_path . '/slick/slick.css');
        drupal_add_css($slick_path . '/slick/slick-theme.css');
        drupal_add_js($slick_path . '/slick/slick.min.js');
      }
      else {
        $error_message = $library['error message'];
        drupal_set_message($error_message, 'error');
      }
    }

    // Expose image with specific image style to template file
    $img_path = image_style_url('vacory_news_image', $variables['field_vactory_image'][0]['uri']);
    $img_width = $variables['field_vactory_image'][0]['width'];
    $img_height = $variables['field_vactory_image'][0]['height'];
    $img = theme('image_style', array(
        'style_name' => 'vacory_news_image',
        'path' => $variables['field_vactory_image'][0]['uri'],
        'width' => $img_width,
        'height' => $img_height,
        'attributes' => array('class' => 'my-image-class'),
      )
    );
    $variables['visual'] = $img;


    // Expose image using picture and breakpoints
    // Get node fields.
    $wrapper = entity_metadata_wrapper('node', $variables['node']);

    // Prepare Image style.
    $image = $wrapper->field_vactory_image->value();
    $image_output = '';
    if ($image && isset($image['uri'])) {
      // Get picture mapping.
      $picture_name = 'vactory_image_medium';
      $fallback_image_style = 'vactory_image_mediumcustom_user_large_1x';
      $picture_mappings = picture_mapping_load($picture_name);
      $breakpoint_styles = picture_get_mapping_breakpoints($picture_mappings, $fallback_image_style);

      // Picture theme.
      $picture = array(
        '#theme' => 'picture',
        '#width' => isset($image['width']) ? $image['width'] : NULL,
        '#height' => isset($image['height']) ? $image['height'] : NULL,
        '#style_name' => $fallback_image_style,
        '#breakpoints' => $breakpoint_styles,
        '#uri' => $image['uri'],
        '#alt' => isset($image['alt']) ? $image['alt'] : '',
        '#attributes' => isset($image['attributes']) ? $image['attributes'] : NULL,
        '#timestamp' => $image['timestamp'],
      );

      // Render picture.
      $image_output = render($picture);
    }
    $variables['visual'] = $image_output;




  }
}
