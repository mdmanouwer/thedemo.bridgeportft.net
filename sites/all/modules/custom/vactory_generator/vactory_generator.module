<?php

/**
 * @file
 * Code for the Vactory Skeleton feature.
 */

include_once 'includes/vactory_generator.utilities.inc';

include_once 'includes/vactory_generator.views.inc';

include_once 'includes/vactory_generator.theme.inc';

/**
 * Hook_permission implementation.
 */
function vactory_generator_permission() {
  $permissions = array();
  $permissions['administer vactory generator'] = array(
    'title'       => t('Administer vactory generator'),
    'description' => t('Administer vactory generator module'),
  );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function vactory_generator_menu() {
  $items = array();

  $items['admin/config/vactory/vactory_generator'] = array(
    'title'            => 'Vactory Generator configuration',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('vactory_generator_admin_settings'),
    'access arguments' => array('administer vactory generator'),
    'file'             => 'includes/vactory_generator.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_entity_info_alter().
 */
function vactory_generator_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['vactory_gen_listing_1_col'] = array(
    'label'           => t('Vactory Generator listing 1 col'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_listing_2_col'] = array(
    'label'           => t('Vactory Generator listing 2 col'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_listing_3_col'] = array(
    'label'           => t('Vactory Generator listing 3 col'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_2_col'] = array(
    'label'           => t('Vactory Generator block 2 col'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_3_col'] = array(
    'label'           => t('Vactory Generator block 3 col'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_1b_2s'] = array(
    'label'           => t('Vactory Generator block_1b_2s'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_1b_3s'] = array(
    'label'           => t('Vactory Generator block_1b_3s'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_3s'] = array(
    'label'           => t('Vactory Generator block_3s'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_5s'] = array(
    'label'           => t('Vactory Generator block_5s'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_slider_small'] = array(
    'label'           => t('Vactory Generator block slider small'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_slider_large'] = array(
    'label'           => t('Vactory Generator block slider large'),
    'custom settings' => FALSE,
  );
  $entity_info['node']['view modes']['vactory_gen_block_masonry'] = array(
    'label'           => t('Vactory Generator block masonry'),
    'custom settings' => FALSE,
  );
}

/**
 * Implements hook_preprocess_node().
 */
function vactory_generator_preprocess_node(&$variables) {

  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  $enabled_content_types = array(
    'vactory_news',
    'vactory_announcements',
    'vactory_event',
    'vactory_partner',
    'vactory_publication',
    'vactory_media',
    'vactory_testimony'
    );

  if (in_array($variables['type'], $enabled_content_types)) {

    // Masonry.
    if ($variables['view_mode'] == 'vactory_gen_block_masonry') {
      if (($library = libraries_detect('masonry')) && !empty($library['installed'])) {
        // Load Masonry assets.
        libraries_load('masonry');
        $path = libraries_get_path('masonry-layout');
        drupal_add_js($path . '/dist/masonry.pkgd.min.js');
      }
      else {
        $error_message = $library['error message'];
        drupal_set_message($error_message, 'error');
      }
    }

    // Slick.
    if (
      $variables['view_mode'] == 'vactory_gen_block_slider_large' ||
      $variables['view_mode'] == 'vactory_gen_block_slider_small'
    ) {
      if (($library = libraries_detect('slick')) && !empty($library['installed'])) {
        // Load Masonry assets.
        libraries_load('slick');
        $slick_path = libraries_get_path('slick');
        drupal_add_css($slick_path . '/slick/slick.css');
        drupal_add_css($slick_path . '/slick/slick-theme.css');
        drupal_add_js($slick_path . '/slick/slick.min.js');
      }
      else {
        $error_message = $library['error message'];
        drupal_set_message($error_message, 'error');
      }
    }

    if ($variables['type'] != 'vactory_media') {
      // Set image
      // Get node fields.
      $wrapper = entity_metadata_wrapper('node', $variables['node']);

      // Prepare Image style.
      $image = $wrapper->field_vactory_image->value();
      $variables['visual'] =  $image;
    }

  }
}

/**
 * Implements template_preprocess_views_view_unformatted.
 */
function vactory_generator_preprocess_views_view_unformatted(&$vars) {
  // Change row layout for both "block_1b_2s" && "block_1b_3s" displays.
  // The first row should have different variant.
  $view = $vars['view'];

  if (strpos($view->name, 'vactory_gen_') === 0 && ($view->current_display == 'block_1b_3s')) {
    if (isset($vars['rows'][0])) {
      $vars['rows'][0] = str_replace("standing-layout", "inline-layout", $vars['rows'][0]);
    }
  }

  if (strpos($view->name, 'vactory_gen_') === 0 && ($view->current_display == 'block_1b_2s')) {
    if (isset($vars['rows'][1])) {
      $vars['rows'][1] = str_replace("standing-layout", "inline-layout is-narrow use-background-thumbnail", $vars['rows'][0]);
    }

    if (isset($vars['rows'][2])) {
      $vars['rows'][2] = str_replace("standing-layout", "inline-layout is-narrow use-background-thumbnail", $vars['rows'][0]);
    }
  }
}
