<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php
 drupal_add_css(drupal_get_path('module', 'vactory_testimonials').'/app/css/styles.css');
?>
<article class="article-card inline-layout <?php print $classes; ?>"<?php print $attributes; ?>>
<div class="row terstimonial-full-row">
    <div class="col-sm-6 first-column">
     <div class="row left-part">
       <div class="col-sm-12 fn-cty ">
            <h5 class="function"><?php  print $node->field_vactory_testimony_function['und'][0]['value'];?></h5> 
           <h5 class="city"><?php print $node->field_vactory_testimony_city['und'][0]['value'];?></h5>
            </div>
        
       <div class="col-sm-12 col-xs-12 paragraph">
   
        <?php if (!empty($title)): ?>
    <h3 class="article-card__title"<?php print $title_attributes; ?>><a
        href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>
        
          <div class="paragraph">
        <?php print substr($node->field_vactory_chapo['und'][0]['value'],0,100)?>

    <a class="permalink read-more"
       href="<?php print $node_url; ?>"><?php print t('Read more'); ?>
    </a></div>
  
           </div>
        </div>
        </div>
    
    
    <div class="col-sm-6  slick-image-full">

    
    
       <?php
      $image_uri = $node->field_vactory_image['und'][0]['uri'];
      $image_url_with_style = image_style_url('vactory_full_slider',$image_uri);
      echo '<img  class="img-responsive" src="'.$image_url_with_style.'">';
      ?>
        </div>
    
    
    
    
    

</div>
</article>
