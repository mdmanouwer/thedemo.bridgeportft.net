<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="container">
  <div class="row">
    <article
      class="article-details col-md-12 <?php print $classes; ?>"<?php print $attributes; ?>>
      <?php if (!empty($title)): ?>
        <div
          class="article-details__title">
          <h2<?php print $title_attributes; ?>><a
              href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
        </div>
      <?php endif; ?>

      <div class="inline-blocks">
        <?php if (isset($content['field_vactory_news_theme'])): ?>
          <div
            class="article-card__tags"><?php print render($content['field_vactory_news_theme']); ?></div>
        <?php endif; ?>
        <?php if (isset($content['field_vactory_date'])): ?>
          <div
            class="article-card__date"><?php print render($content['field_vactory_date']); ?></div>
        <?php endif; ?>
      </div>
      <?php /* ?>
      <div class="article-details-meta">
        <div
          class="article-details__tags"><?php print render($content['field_vactory_news_theme']); ?></div>

        <div
          class="article-details__date"><?php print render($content['field_vactory_date']); ?></div>
      </div>
 <?php */ ?>

      <?php if (isset($node->body['und'][0]['summary']) && !empty($node->body['und'][0]['summary'])): ?>
        <div class="article-details__summary">
          <?php print $node->body['und'][0]['summary']; ?>
        </div>

      <?php endif; ?>
<?php /* ?>
      <div class="article-details__poster">
        <?php print v_gen_render_image($visual, $picture_name = 'vactory_node_wide', $image_style_fallback = 'node-wide-fullcustom_user_large_1x'); ?>
      </div>
 <?php */ ?>
      <div class="article-details__body">
        <?php print render($content['body']); ?>
      </div>

    </article>
  </div>
</div>
