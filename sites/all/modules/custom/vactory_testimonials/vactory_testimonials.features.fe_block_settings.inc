<?php
/**
 * @file
 * vactory_testimonials.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function vactory_testimonials_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-d705c9a8bc79ca76b2760ffa9b8db187'] = array(
    'cache' => -1,
    'css_class' => '',
    'delta' => 'd705c9a8bc79ca76b2760ffa9b8db187',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'radix' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'radix',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'starter1' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'starter1',
        'weight' => 0,
      ),
    ),
  );

  return $export;
}
