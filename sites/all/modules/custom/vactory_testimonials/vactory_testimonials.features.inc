<?php
/**
 * @file
 * vactory_testimonials.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_testimonials_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function vactory_testimonials_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function vactory_testimonials_node_info() {
  $items = array(
    'vactory_testimony' => array(
      'name' => t('vactory_testimony'),
      'base' => 'node_content',
      'description' => t('This type of content can be used for a testimonial of a collaborator or a client'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
