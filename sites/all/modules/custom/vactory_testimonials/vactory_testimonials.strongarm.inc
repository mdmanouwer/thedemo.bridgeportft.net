<?php
/**
 * @file
 * vactory_testimonials.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function vactory_testimonials_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_vactory_testimony';
  $strongarm->value = 0;
  $export['comment_anonymous_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_vactory_testimony';
  $strongarm->value = 0;
  $export['comment_default_mode_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_vactory_testimony';
  $strongarm->value = '10';
  $export['comment_default_per_page_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_vactory_testimony';
  $strongarm->value = 0;
  $export['comment_form_location_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_vactory_testimony';
  $strongarm->value = '0';
  $export['comment_preview_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_vactory_testimony';
  $strongarm->value = 0;
  $export['comment_subject_field_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_vactory_testimony';
  $strongarm->value = '1';
  $export['comment_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__vactory_testimony';
  $strongarm->value = array(
    'view_modes' => array(
      'vactory_gen_listing_1_col' => array(
        'custom_settings' => TRUE,
      ),
      'vactory_gen_listing_2_col' => array(
        'custom_settings' => TRUE,
      ),
      'vactory_gen_listing_3_col' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_2_col' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_3_col' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_3s' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_5s' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_1b_3s' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_1b_2s' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_slider_small' => array(
        'custom_settings' => FALSE,
      ),
      'vactory_gen_block_slider_large' => array(
        'custom_settings' => TRUE,
      ),
      'vactory_gen_block_masonry' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '3',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '1',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_vactory_testimony';
  $strongarm->value = '2';
  $export['language_content_type_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_vactory_testimony';
  $strongarm->value = array();
  $export['menu_options_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_vactory_testimony';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_vactory_testimony';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_vactory_testimony';
  $strongarm->value = '0';
  $export['node_preview_vactory_testimony'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_vactory_testimony';
  $strongarm->value = 0;
  $export['node_submitted_vactory_testimony'] = $strongarm;

  return $export;
}
