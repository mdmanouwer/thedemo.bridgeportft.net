/**
 * @file
 * Js file for vactory_testimonials.
 *
 */
(function ($) {
  Drupal.behaviors.vactory_testimonials = {
    attach: function (context, settings) {
         var is_rtl = ($('html[dir="rtl"]').length) ? true : false;
        // Slick big slides slider
        $('.block-slider-large-vactory-testimony .slider-grid').each(function() {
            var $self = $(this);

            if ($self.children().length == 1) {
                $self.addClass('single-item');
            }
        });


        $('.block-slider-large-vactory-testimony .slider-grid').slick({
            dots: true,
            arrows: true,
            infinite: true,
            cssEase: 'cubic-bezier(0.585, -0.005, 0.635, 0.920)',
            useTransform: true,
            speed: 800,
            rtl: is_rtl,
            appendDots: $('.block-slider-large-vactory-testimony .view-content'),
            nextArrow: '<button type="button" class="slick-arrow next ripple-effect"><i class="icon-arrow-right-flat"></i></button>',
            prevArrow: '<button type="button" class="slick-arrow prev ripple-effect"><i class="icon-arrow-left-flat"></i></button>'
        });


    }
  };
}(jQuery));
