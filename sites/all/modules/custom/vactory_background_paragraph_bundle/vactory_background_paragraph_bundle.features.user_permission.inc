<?php
/**
 * @file
 * vactory_background_paragraph_bundle.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vactory_background_paragraph_bundle_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create paragraph content paragraph_background'.
  $permissions['create paragraph content paragraph_background'] = array(
    'name' => 'create paragraph content paragraph_background',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'delete paragraph content paragraph_background'.
  $permissions['delete paragraph content paragraph_background'] = array(
    'name' => 'delete paragraph content paragraph_background',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'update paragraph content paragraph_background'.
  $permissions['update paragraph content paragraph_background'] = array(
    'name' => 'update paragraph content paragraph_background',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content paragraph_background'.
  $permissions['view paragraph content paragraph_background'] = array(
    'name' => 'view paragraph content paragraph_background',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
