/**
 * @file
 * Javascript behaviors for the Vactory_blog module.
 */

// Facebook load Javascript SDK
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Drupal Javascript behaviors.
(function ($) {

    // Detect whatever the user is connected using Facebook.
    window.is_user_fb_connected = (document.cookie.match(/^(.*;)?facebook_user=[^;]+(.*)?$/)) ? true : false;
    window.user_fb_name = '';
    // Facebook dom container.
    var facebook_container = $('#vactory-blog--facebook-connect');
    // Load dom elements.
    var dom = init_vars();

    /**
     * Init variables
     *
     * @returns {}
     */
    function init_vars() {
        return {
            container: facebook_container,
            userInfo: facebook_container.find('.vactory-blog--user-info'),
            login: facebook_container.find('.vactory-blog--login'),
            logout: facebook_container.find('.vactory-blog--logout'),
            loginBtn: $('#vactory-blog--login'),
            logoutBtn: $('#vactory-blog--logout'),
            commentFormFields: $('#comments .form-item-mail, #comments .form-item-name, #vactory-blog--reply-form .form-item-mail, #vactory-blog--reply-form .form-item-name'),
            commentUserField: $('[name="name"]'),
            commentEmailField: $('[name="mail"]'),
            facebookCheckbox: $('[name*="facebook_user"]')
            //facebookYes: $('[id^="edit-facebook-user-yes"]'), // Ajax form reload, may change to edit-facebook-user-yes--2
            //facebookNo: $('[id^="edit-facebook-user-no"]')
        };
    }

    /**
     * Reinitialize variables.
     */
    function reinit_vars() {
        dom = init_vars();
    }

    /**
     * Drupal Vactory Blog Behavior
     */
    Drupal.behaviors.vactoryBlogComments = {
        attach: function (context) {

            // If ajax happens (while fb user connected) and this behavior got called again with a refreshed form
            // call form_states again.
            if (is_user_fb_connected) {
                form_states();
            }
            else {
                form_states_defaults();
            }

            // Repload reCaptcha on each request.
            if (typeof grecaptcha == "object") {
                $('.g-recaptcha').each(function () {
                    var $self = $(this);
                    $self.empty();
                    if ($self.data("captcha") == undefined) {
                        try {
                            var id = grecaptcha.render($self.get(0), {
                                'sitekey': $self.data('sitekey')
                            });
                            $self.data("captcha", id);
                        } catch(e) {
                            grecaptcha.reset();
                        }
                    }
                    else {
                        grecaptcha.reset($self.data("captcha"));
                    }
                });
            }

            // Facebook user profile theme prototype.
            Drupal.theme.prototype.vactoryBlogFbUserProfile = function(href, text) {
                return '<a href="' + href + '">' + text + '</a>';
            };

            if (context == document) {

                // Attach click events to FB buttons (login/logout)
                dom.loginBtn.click(vactoryBlog_login);
                dom.logoutBtn.click(vactoryBlog_logout);

                // Await for facebook callback.
                window.fbAsyncInit = function() {
                    FB.init({
                        appId: Drupal.settings.vactory_blog.appid,
                        xfbml: true,
                        version: 'v2.5',
                        cookie: true
                    });

                    FB.getLoginStatus(function(response) {
                        if (response.status === 'connected') {
                            vactoryBlog_getInfo();
                        }
                    });
                }; // window.fbAsyncInit();

            } // (context == document)
        }
    };

    /**
     * Helper functions
     *
     * Core facebook SDK utilites
     */

    // Login with facebook with extra permissions
    function vactoryBlog_login() {
        FB.login(function (response) {
            if (response.status === 'connected') {
                (function(key, value, hoursExpire) {
                    var ablauf = new Date();
                    var expireTime = ablauf.getTime() + (hoursExpire * 60 * 60 * 1000);
                    ablauf.setTime(expireTime);
                    document.cookie = key + "=" + value + "; expires=" + ablauf.toGMTString();
                }('facebook_user', 'true', 60));

                vactoryBlog_getInfo(); // Reset form to Facebook users
            }
        }, {
            scope: 'email'
        });
    }

    // Facebook logout
    function vactoryBlog_logout() {
        FB.logout(function(response) {
            document.cookie = 'facebook_user' + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            is_user_fb_connected = false;
            form_states_defaults(); // Reset form to Anonymous users
        });
    }


    // Enable facebook
    window.form_states = function () {
        reinit_vars();
        if (window.user_fb_name.length > 0) {
            dom.commentUserField.val(window.user_fb_name);
        }
        dom.login.addClass('hidden');
        dom.logout.removeClass('hidden');
        dom.commentFormFields.addClass('hidden');
        dom.facebookCheckbox.prop('checked', false);
        dom.facebookCheckbox.trigger('change');
        dom.facebookCheckbox.prop('checked', true);
        dom.facebookCheckbox.trigger('change');
    };

    // Anonymous users
    window.form_states_defaults = function () {
        reinit_vars();
        if (window.user_fb_name.length > 0) {
            dom.commentUserField.val(window.user_fb_name);
        }
        dom.login.removeClass('hidden');
        dom.logout.addClass('hidden');
        dom.commentFormFields.removeClass('hidden');
        dom.facebookCheckbox.prop('checked', true);
        dom.facebookCheckbox.trigger('change');

        dom.facebookCheckbox.prop('checked', false);
        dom.facebookCheckbox.trigger('change');
    };

    // Getting basic user info
    function vactoryBlog_getInfo() {
        // Mark user as connected using Facebook.
        is_user_fb_connected = true;
        form_states();

        FB.api('/me', 'GET', {
            fields: 'name,id,link,email'
        }, function (response) {
            // Render user info.
            dom.userInfo.html(Drupal.t("Connected as") + ' ' + Drupal.theme('vactoryBlogFbUserProfile', response.link, response.name));
            // Fill name & email fields.
            window.user_fb_name = response.name;
            dom.commentUserField.val(response.name);
            dom.commentEmailField.val(response.email);
        });
    }


})(jQuery);
