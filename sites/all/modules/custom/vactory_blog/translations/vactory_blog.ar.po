# $Id$
#
# Arabic translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  vactory_blog.features.field_instance.inc: n/a
#  vactory_blog.features.inc: n/a
#  include/vactory_blog.admin.inc: n/a
#  vactory_blog.views_default.inc: n/a
#  templates/views/views-view-fields--page.tpl.php: n/a
#  vactory_blog.module: n/a
#  vactory_blog.info: n/a
#  js/facebook-connect--comments.js: n/a
#  templates/comments/better-comments-wrapper.tpl.php: n/a
#  templates/node/node--vactory_blog_entry.tpl.php: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-02-10 18:38+0100\n"
"PO-Revision-Date: 2017-02-10 17:52+0000\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: Arabic <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=((n==1)?(0):((n==0)?(1):((n==2)?(2):((((n"
"%100)>=3)&&((n%100)<=10))?(3):((((n%100)>=11)&&((n%100)<=99))?(4):5)))));\n"
"Language: ar\n"
"X-Generator: Poedit 1.8.11\n"

#: vactory_blog.features.field_instance.inc:468
msgid "Author"
msgstr "المؤلف"

#: vactory_blog.features.field_instance.inc:469
msgid "Body"
msgstr "المتن"

#: vactory_blog.features.field_instance.inc:470
msgid "Description"
msgstr "الوصف"

#: vactory_blog.features.field_instance.inc:471
msgid "Files"
msgstr "الملفات"

#: vactory_blog.features.field_instance.inc:472
msgid "Image"
msgstr "الصورة"

#: vactory_blog.features.field_instance.inc:473
msgid "Picto"
msgstr "الصورة"

#: vactory_blog.features.field_instance.inc:474
msgid "Taxonomy"
msgstr "التصنيف"

#: vactory_blog.features.field_instance.inc:475
msgid "Video"
msgstr "فيديو"

#: vactory_blog.features.inc:70
msgid "Vactory Blog entry"
msgstr "مدونة"

#: vactory_blog.features.inc:74 include/vactory_blog.admin.inc:48
msgid "Title"
msgstr "العنوان"

#: vactory_blog.views_default.inc:326
msgid "Master"
msgstr "الرئيسي"

#: vactory_blog.views_default.inc:327
msgid "Blog"
msgstr "المدونة"

#: vactory_blog.views_default.inc:328
msgid "more"
msgstr "المزيد"

#: vactory_blog.views_default.inc:329
msgid "Apply"
msgstr "تطبيق"

#: vactory_blog.views_default.inc:330
msgid "Reset"
msgstr "إعادة الضبط"

#: vactory_blog.views_default.inc:331
msgid "Sort by"
msgstr "الترتيب حسب"

#: vactory_blog.views_default.inc:332
msgid "Asc"
msgstr "تصاعدي"

#: vactory_blog.views_default.inc:333
msgid "Desc"
msgstr "تنازلي"

#: vactory_blog.views_default.inc:334
msgid "Advanced options"
msgstr "الخيارات المتقدمة"

#: vactory_blog.views_default.inc:335
msgid "Filtrer by taxonomy"
msgstr "تصفية حسب التصنيف"

#: vactory_blog.views_default.inc:336
msgid "Filtrer by Author"
msgstr "تصفية حسب الكاتب"

#: vactory_blog.views_default.inc:337
msgid "Select any filter and click on Apply to see results"
msgstr ""

#: vactory_blog.views_default.inc:338
msgid "Items per page"
msgstr ""

#: vactory_blog.views_default.inc:339
msgid "- All -"
msgstr "- الكل -"

#: vactory_blog.views_default.inc:340
msgid "Offset"
msgstr ""

#: vactory_blog.views_default.inc:341
msgid "« first"
msgstr "« الأولى"

#: vactory_blog.views_default.inc:342
msgid "‹ previous"
msgstr "‹ السابقة"

#: vactory_blog.views_default.inc:343
msgid "next ›"
msgstr "التالية ›"

#: vactory_blog.views_default.inc:344
msgid "last »"
msgstr "الأخيرة »"

#: vactory_blog.views_default.inc:345 templates/views/views-view-fields--page.tpl.php:56
msgid "Published by"
msgstr "نشرت من قبل"

#: vactory_blog.views_default.inc:346
msgid "on"
msgstr "على"

#: vactory_blog.views_default.inc:347
msgid "Sort"
msgstr "فرز"

#: vactory_blog.views_default.inc:348
msgid "Page"
msgstr "الصفحة"

#: vactory_blog.module:40
msgid "This is a multilingual form. Make sure to fill it in the other languages"
msgstr "هذه الاستمارة متعددة اللغات. المرجو ملء الاستمارة لجميع اللغات."

#: vactory_blog.module:66
msgid "Administer Vactory Blog"
msgstr "إدارة المدونة"

#: vactory_blog.module:67
msgid "Access vactory blog settings pages."
msgstr "إعدادات الوصول بلوق الصفحات."

#: vactory_blog.module:171;384 templates/views/views-view-fields--page.tpl.php:58
msgid "at"
msgstr "في"

#: vactory_blog.module:249
msgid "Yes"
msgstr "نعم"

#: vactory_blog.module:249
msgid "No"
msgstr "لا"

#: vactory_blog.module:264
msgid "E-mail"
msgstr "البريد الإلكتروني"

#: vactory_blog.module:274
msgid "The content of this field is kept private and will not be shown publicly."
msgstr "محتويات هذا الحقل سرية ولن تظهر للآخرين."

#: vactory_blog.module:293
msgid "Cancel"
msgstr "إلغاء"

#: vactory_blog.module:357
msgid "The e-mail address field is required."
msgstr "مطلوب حقل عنوان البريد الإلكتروني."

#: vactory_blog.module:361
msgid "The e-mail address you specified is not valid."
msgstr "عنوان البريد الإلكتروني المحدد غير صالح."

#: vactory_blog.module:511
msgid "Filter by year"
msgstr "تصفية حسب السنة"

#: vactory_blog.module:521
msgid "Filter by month"
msgstr "تصفية حسب الشهر"

#: vactory_blog.module:48 vactory_blog.info:0
msgid "Vactory Blog"
msgstr ""

#: vactory_blog.module:49
msgid "Configure listing blog"
msgstr ""

#: vactory_blog.info:0
msgid "Vactory blog feature"
msgstr ""

#: vactory_blog.info:0
msgid "Features"
msgstr "الخصائص"

#: include/vactory_blog.admin.inc:31
msgid "Configuration links"
msgstr "روابط الإعدادات"

#: include/vactory_blog.admin.inc:34
msgid "Sort Blog Entries"
msgstr "ترتيب مقالات المدونة"

#: include/vactory_blog.admin.inc:43
msgid "Blog Listing"
msgstr "قائمة المدونة"

#: include/vactory_blog.admin.inc:54
msgid "Facebook Appid"
msgstr ""

#: include/vactory_blog.admin.inc:60
msgid "Header content"
msgstr "محتوى رأس الصفحة"

#: include/vactory_blog.admin.inc:67
msgid "Footer content"
msgstr "محتوى أسفل الصفحة"

#: include/vactory_blog.admin.inc:74
msgid "Submit"
msgstr "إضافة"

#: include/vactory_blog.admin.inc:95
msgid "Successfully updated settings."
msgstr "تم حفظ الإعدادات بنجاح."

#: js/facebook-connect--comments.js:0
msgid "Connected as"
msgstr "متصل باسم"

#: templates/comments/better-comments-wrapper.tpl.php:42
msgid "@nb_comments Comments"
msgstr "‫@nb_comments تعليق"

#: templates/comments/better-comments-wrapper.tpl.php:50
msgid "Login with Facebook"
msgstr "تسجيل الدخول باستخدام الفيسبوك"

#: templates/comments/better-comments-wrapper.tpl.php:53
msgid "Disconnect"
msgstr ""
"‫Translate‬\n"
"\n"
"‏‫Disconnect‬\n"
"\n"
"‫10/5000‬\n"
"‫قطع الاتصال"

#: templates/comments/better-comments-wrapper.tpl.php:73
msgid "Reply"
msgstr "الرد على التعليق"

#: templates/node/node--vactory_blog_entry.tpl.php:81
msgid "Published on"
msgstr "نشر في"

#: templates/views/views-view-fields--page.tpl.php:52
msgid "Views :"
msgstr "عدد المشاهدات :"
