<?php

/**
 * @file
 * Administrative page callbacks for the vactory_blog module.
 */

/**
 * Admin settings form.
 *
 * @param array $form
 *   Form API form.
 * @param array $form_state
 *   Form API form.
 * @param bool $no_js_use
 *   Used for this demonstration only. If true means that the form should be
 *   built using a simulated no-javascript approach (ajax.js will not be
 *   loaded.)
 *
 * @return array
 *   Form array.
 */
function vactory_blog_admin_settings(array $form, array &$form_state, $no_js_use = FALSE) {
  _vactory_blog_set_admin_form_messages();

  $default_value = variable_get(_vactory_blog_get_variable_name(), array());

  $form['links'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Configuration links'),
  );

  $form['links']['link_draggable_entries'] = array('#markup' => l(t('Sort Blog Entries'), 'admin/vactory_blog/draggableviews/entries'));

  if (empty($default_value['vactory_blog']['title'])) {
    $default_value['vactory_blog']['title'] = 'Blog';
  }

  $form['vactory_blog'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Blog Listing'),
  );

  $form['vactory_blog']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => (isset($default_value['vactory_blog'])) ? $default_value['vactory_blog']['title'] : '',
  );

  $form['vactory_blog']['facebook_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Appid'),
    '#default_value' => (isset($default_value['vactory_blog']) && isset($default_value['vactory_blog']['facebook_appid'])) ? $default_value['vactory_blog']['facebook_appid'] : '',
  );

  $form['vactory_blog']['header'] = array(
    '#type' => 'text_format',
    '#title' => t('Header content'),
    '#format' => filter_default_format(),
    '#default_value' => (isset($default_value['vactory_blog']['header'])) ? $default_value['vactory_blog']['header']['value'] : '',
  );

  $form['vactory_blog']['footer'] = array(
    '#type' => 'text_format',
    '#title' => t('Footer content'),
    '#format' => filter_default_format(),
    '#default_value' => (isset($default_value['vactory_blog']['footer'])) ? $default_value['vactory_blog']['footer']['value'] : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Add a submit handler/function to the form.
 *
 * This will save data and add a completion message to the screen when the
 * form successfully processes.
 */
function vactory_blog_admin_settings_submit($form, &$form_state) {

  $data['vactory_blog'] = array(
    'title' => $form_state['values']['vactory_blog']['title'],
    'facebook_appid' => $form_state['values']['vactory_blog']['facebook_appid'],
    'header' => $form_state['values']['vactory_blog']['header'],
    'footer' => $form_state['values']['vactory_blog']['footer'],
  );

  variable_set(_vactory_blog_get_variable_name(), $data);
  drupal_set_message(t('Successfully updated settings.'));
}
