<?php

/**
 * @file
 * vactory_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_blog_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function vactory_blog_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function vactory_blog_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['l j F Y - h:i'] = 'l j F Y - h:i';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function vactory_blog_date_format_types() {
  $format_types = array();
  // Exported date format type: blog_entry
  $format_types['blog_entry'] = 'blog_entry';
  return $format_types;
}

/**
 * Implements hook_fe_date_locale_date_format().
 */
function vactory_blog_fe_date_locale_date_format() {
  $locale_date_formats = array();

  // Exported format: blog_entry::eng
  $locale_date_formats['blog_entry::eng'] = array(
    'type' => 'blog_entry',
    'format' => 'l j F Y - h:i',
    'locales' => array(
      0 => 'eng',
    ),
  );
  return $locale_date_formats;
}

/**
 * Implements hook_image_default_styles().
 */
function vactory_blog_image_default_styles() {
  $styles = array();

  // Exported image style: vactory_blog_author_picto.
  $styles['vactory_blog_author_picto'] = array(
    'label' => 'Vactory Blog Author Picto',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 262,
          'height' => 262,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_blog_entries.
  $styles['vactory_blog_entries'] = array(
    'label' => 'Vactory Blog Entries',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 400,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_blog_image_cover_big.
  $styles['vactory_blog_image_cover_big'] = array(
    'label' => 'Blog image cover big (2040x900)',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 2040,
          'height' => 900,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_blog_image_cover_small.
  $styles['vactory_blog_image_cover_small'] = array(
    'label' => 'Blog image cover small (760x560)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 760,
          'height' => 560,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_blog_media_image.
  $styles['vactory_blog_media_image'] = array(
    'label' => 'Vactory Blog Media Image',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 720,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function vactory_blog_node_info() {
  $items = array(
    'vactory_blog_entry' => array(
      'name' => t('Vactory Blog entry'),
      'base' => 'node_content',
      'description' => t('Used in Vactory Blog module.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
