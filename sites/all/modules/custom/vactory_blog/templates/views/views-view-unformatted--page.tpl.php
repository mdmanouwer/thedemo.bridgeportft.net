<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="row">
  <div class="row-flex-wrapper">
    <?php foreach ($rows as $id => $row): ?>
      <div<?php if ($classes_array[$id]) {
        print ' class="' . $classes_array[$id] . ' col-md-4 entry-item "';
      } ?>>
        <?php print $row; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
