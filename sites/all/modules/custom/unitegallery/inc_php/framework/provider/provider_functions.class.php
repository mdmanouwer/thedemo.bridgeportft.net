<?php

defined('_JEXEC') or die('Restricted access');

define("UNITEGALLERY_TEXTDOMAIN","unitegallery");

class UniteProviderFunctionsUG{

	
	/**
	 * get current website prefix
	 */
	private static function getDatabasePrefix(){
		global $databases;
		if(empty($databases))
			return('');
		
		foreach($databases as $arr){
			if(empty($arr))
				return('');
			
			foreach($arr as $config){
				if(empty($config))
					return('');
				
				if(!is_array($config))
					return("");
				
				$prefix = UniteFunctionsUG::getVal($config, "prefix");
				
				return($prefix);
			}
		}
		
		return("");
	}
	
	
	/**
	 * init base variables of the globals
	 */
	public static function initGlobalsBase(){
    	
		global $base_url;
				
		//$tablePrefix = self::getDatabasePrefix();
		$tablePrefix = "";
		
		GlobalsUG::$table_galleries = "{".GlobalsUG::TABLE_GALLERIES_NAME."}";
		GlobalsUG::$table_categories = "{".GlobalsUG::TABLE_CATEGORIES_NAME."}";
		GlobalsUG::$table_items = "{".GlobalsUG::TABLE_ITEMS_NAME."}";
				
		$pluginName = "unitegallery";
		
		GlobalsUG::$pathPlugin = realpath(drupal_get_path('module', 'unitegallery'))."/";

		GlobalsUG::$path_media_ug = GlobalsUG::$pathPlugin."unitegallery-plugin/";
    
	    $wrapper = file_stream_wrapper_get_instance_by_uri('public://');
	    
	    $path = $wrapper->realpath();
	    
		//check if there is ? in the url
		$hasQM = false;
		$uri = UniteFunctionsUG::getVal($_SERVER, "REQUEST_URI");
		if(strpos($uri,"?") !== false)
			$hasQM = true;
		
		$baseURL = $base_url."/";
		if($hasQM == true)
			$baseURL .= "?q=";
		
	    //dmp($path);exit();
	    
		GlobalsUG::$path_images = $path;
		GlobalsUG::$path_cache = GlobalsUG::$pathPlugin."cache/";

		GlobalsUG::$urlPlugin = file_create_url(drupal_get_path('module', 'unitegallery'))."/";
		
		GlobalsUG::$url_component_client = "";
		 
		GlobalsUG::$url_component_admin = $baseURL."admin/config/media/".$pluginName;
		
		GlobalsUG::$url_base = $base_url."/";
				
		GlobalsUG::$url_media_ug = GlobalsUG::$urlPlugin."unitegallery-plugin/";
    
		GlobalsUG::$url_images = file_create_url('public://');

		$querySap = "?";
		if($hasQM == true)
			$querySap = "&";
		 
		GlobalsUG::$url_ajax = GlobalsUG::$url_component_admin.$querySap."ajax=1";
		
		GlobalsUG::$url_ajax_front = $baseURL."unitegallery-front";
		
	}
	
	
	/**
	 * add scripts and styles framework
	 */
	public static function addScriptsFramework(){
		
		HelperUG::addScriptCommon("jquery-ui.min","jquery-ui");
		HelperUG::addStyle("jquery-ui.structure.min","jui-smoothness-structure","css/jui/new");
		HelperUG::addStyle("jquery-ui.theme.min","jui-smoothness-theme","css/jui/new");
	}
	
	
	/**
	 *
	 * register script
	 */
	public static function addScript($handle, $url){
	    	    
		if(empty($url))
			UniteFunctionsUG::throwError("empty script url, handle: $handle");
	    
    	drupal_add_js($url, 'external');
	}
	
	
	/**
	 *
	 * register script
   * EDIT
	 */
	public static function addStyle($handle, $url){
	
		if(empty($url))
			UniteFunctionsUG::throwError("empty style url, handle: $handle");
	
    	drupal_add_css($url, 'external');
			
	}
	
	
	/**
	 * add inline style
	 * 
	 */
	public static function addStyleInline($style){
		
		drupal_add_css($style, "inline");
		
	}	
	
	
	/**
	 *
	 * sanitize data, in drupal no need to sanitize
	 */
	public static function normalizeAjaxInputData($arrData){
		
		return $arrData;
	}
		
	
	/**
	 * put footer text line
	 */
	public static function putFooterTextLine(){
		?>
			&copy; <?php _e("All rights reserved",UNITEGALLERY_TEXTDOMAIN)?>, <a href="http://codecanyon.net/user/valiano?ref=valiano" target="_blank">Valiano</a>. &nbsp;&nbsp;
		<?php
	}
	
	/**
	 * return if the user is on admin part
	 */
	public static function isAdmin(){
		
		$isAdmin = path_is_admin(current_path());
		
		return($isAdmin);
	}
	
	
	/**
	 * add jquery include
	 */
	public static function addjQueryInclude($app, $urljQuery = null){
		
		$isAdmin = self::isAdmin();
				
		if($isAdmin == true)
			return(true);
		
		self::addScript("jquery", $urljQuery);
	}
	
	
	/**
	 * add position settings (like shortcode) based on the platform
	 */
	public static function addPositionToMainSettings($settingsMain){
	
		$textGenerate = __("Generate Shortcode",UNITEGALLERY_TEXTDOMAIN);
		$descShortcode = __("Copy this shortcode into article / page / block content",UNITEGALLERY_TEXTDOMAIN);
		$settingsMain->addTextBox("shortcode", "",__("Gallery Shortcode",UNITEGALLERY_TEXTDOMAIN),array("description"=>$descShortcode, "readonly"=>true, "class"=>"input-alias input-readonly", "addtext"=>"&nbsp;&nbsp; <a id='button_generate_shortcode' class='unite-button-secondary' >{$textGenerate}</a>"));
	
		return($settingsMain);
	}
	
	/**
	 * modify default values of troubleshooter settings
	 */
	public static function modifyTroubleshooterSettings(UniteGallerySettingsUG $settings){
		
		$settings->updateSettingValue("include_jquery", "false");
		
		return($settings);
	}
	
	
	/**
	 * add tile size related settings
	 */
	public static function addTilesSizeSettings($settings){
		
		$arrSizes = array();
		$arrSizes["medium"] = "Medium (max width - 300)";
		$arrSizes["large"] = "Large (max width - 768)";
		$arrSizes["full"] = "Full";
		
		$params = array(
				"description"=>__("Tiles thumbs resolution. If selected 'Large', The thumbnails will be generated on the first gallery output", UNITEGALLERY_TEXTDOMAIN)
		);
		
		$settings->addHr();
		
		$settings->addSelect("tile_image_resolution", $arrSizes, "Tile Image Resolution", "medium", $params);
		
		return($settings);
	}
		
	
	/**
	 * put galleries view text
	 */
	public static function putGalleriesViewText(){
		
		?>
		
		<div class="galleries-view-box" style="">		
			
			<div class="view-box-title">How to use the gallery</div>
			
				<p>
				* From the <b>article</b> or <b> block</b> insert the shortcode from the gallery view. Example: <b>[unitegallery gallery1]</b>
				</p>
				<p>
				* Also, you can put the same gallery, bug with different category. Example: <b>[unitegallery gallery1 catid=2]</b>
				<br>
				&nbsp; There are shortcode generator near the shortcode field that help to generate those types of shortcodes
				</p>
		</div>
		
		<?php
	}
	
	
	/**
	 * print custom script
	 */
	public static function printCustomScript($script, $hardCoded = false){
	
		echo "<script type='text/javascript'>{$script}</script>";
	
	}
	
	
}

//WordPress emulator functions:

	function __($string, $textdomain = null){
			
			return($string);
	}
  
  function _e($string, $textdomain = null){
			
			echo($string);
	}
?>