


//disable opening in overlay

if(window.top != window){
	
	jQuery(document).ready(function(){
		var url = location.href;
		url = url.replace("?render=overlay","");
		
		var replaceHTML = "<div style='height:200px;padding-top:40px;font-size:16px;'>The gallery admin works best from outside the overlay, redirecting...</div>";
		jQuery("#page").html(replaceHTML);
		top.location.href = url;
	});
	
}


var SqueezeBox = new function(){
	
	this.onClose = null;
	
	this.initialize = function(){};
	this.assign = function(){};
	
	
	/**
	 * open fancybox, set onclose function
	 */
	this.open = function(onCloseFunction){
				
		this.onClose = onCloseFunction;
		if(g_urlViewBase.indexOf("?") == -1)			
			var url = g_urlViewBase+"?view=mediaselect";
		else
			var url = g_urlViewBase+"&view=mediaselect";
		
		var options = {};
		options.handler = "iframe";
		options.size = {x:900,y:550};
		
		var objFancybox = jQuery("#fancybox_trigger");
		objFancybox.attr("href",url);
				
		jQuery("#fancybox_trigger").trigger("click");
	};
		
	/**
	 * close fancybox
	 */
	this.close = function(){
				
		jQuery("#fancybox-close").trigger("click");
	};	
};


/**
 * 
 * on inset image, taken from iframe
 */
function jInsertFieldValue(urlImage){
	
	if(typeof SqueezeBox.onClose == "function")
		SqueezeBox.onClose(urlImage);
	
	SqueezeBox.close();
}


/**
 * provider admin class
 */
function UniteProviderAdminUG(){
	
	
	/**
	 * open "add image" dialog
	 */
	this.openAddImageDialog = function(title, onInsert, isMultiple){
		
		SqueezeBox.open(function(urlImage){
			onInsert(urlImage);
		});
		
	};
	
	/**
	 * get shortcode
	 */
	this.getShortcode = function(alias, catid){
		
		var addhtml = "";
		if(catid)
			addhtml = " catid="+catid;
		
		var shortcode = "[unitegallery "+alias + addhtml + "]";
		
		if(alias == "")
			shortcode = "";
		
		return(shortcode);
	};
	
	
	/**
	 * put global error, replace the view
	 */
	function showGlobalError(message){
		
		var message = "<div class='messages error'>" + message + "</div>";
		
		jQuery("#viewWrapper").html(message);
		
	}
	
	
	/**
	 * return if minimal version
	 */
	function minVersion(version) {
		
		  var $vrs = jQuery.fn.jquery.split('.'),
		      min  = version.split('.');
		  
		  for (var i=0, len=$vrs.length; i<len; i++) {			  
		    if (min[i] && parseInt($vrs[i]) < parseInt(min[i])) {
		      return false;
		    }
		  }
		  return true;
	}
	
	/**
	 * call this on global init. If return false - not proceed with the init
	 */
	this.onGlobalInit = function(){
		
		var versionFull = jQuery.fn.jquery;
		
		if(minVersion("1.8.0") == false){
			var errorMessage = "<b>jQuery Version Error</b>: ";
			errorMessage += "The Unite Gallery module will work only from <b> jQuery version 1.8 and higher</b>. Please update the jQuery version on drupal (jquery update module). <br> Now you have <b> jQuery "+versionFull+" </b> version";
			
			showGlobalError(errorMessage);
			
			return(false);
		}
					
		return(true);
	}
	
}

