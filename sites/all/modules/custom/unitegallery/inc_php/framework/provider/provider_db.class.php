<?php

defined('_JEXEC') or die('Restricted access');

class UniteProviderDBUG{
	
	private $last_id;
	
	
	/**
	 *
	 * constructor - set database object
	 */
	public function __construct(){
	}
	
	
	/**
	 * get error number
	 */
	public function getErrorNum(){
		
		$errorNum = Database::getConnection()->errorCode();
		
		return $errorNum;
	}
	
	
	/**
	 * get error message
	 */
	public function getErrorMsg(){
		
		$type = Database::getConnection()->databaseType();
		
		$error = "";
		
		switch($type){
			case "mysql":
				if(function_exists("mysql_error"))
					$error = mysql_error();
			break;
			case "sqlite":
				$error = sqlite_error_string();
			break;
		}
		
		return $error;
	}
	
	/**
	 * get last row insert id
	 */
	public function insertid(){
    
    	return $this->last_id;
	}
	
	
	/**
	 * do sql query, return success
	 */
	public function query($query){
				
    	$lastid = db_query($query, array(), array('return' => Database::RETURN_INSERT_ID));
    	
    	$this->last_id = $lastid;

		return($lastid);
	}
	
		
	
	/**
	 * fetch objects from some sql
	 */
	public function fetchSql($query){
					
	    $rows = db_query($query)->fetchAll();	
		
	    return($rows);
	}
	
	
	
	/**
	 * escape some string
	 * the escaping is running anyway on prepare
	 */
	public function escape($string){
    			
    	return $string;
 	}
 	
 	
 	/**
 	 * update function
 	 */
 	public function update($tableName, $arrData, $where){

 			if(is_array($where) == false)
 				UniteFunctionsUG::throwError("The 'where' ver must be array");
 			
 			if(empty($where))
 				UniteFunctionsUG::throwError("The 'where' array must be not empty");
 			
 			$options = array('return' => Database::RETURN_AFFECTED);
 			
 			$objUpdate = db_update($tableName, $options);
	 		$objUpdate->fields($arrData);
	 				
 			foreach($where as $field => $value)
 				$objUpdate->condition($field, $value, "=");
 			
 			$numRows = $objUpdate->execute();
 			
 			return($numRows);
 	}
 	
 	
 	/**
 	 * insert query
 	 */
 	/*
 	public function insert($tableName, $arrData){
 		
 		$options = array('return' => Database::RETURN_INSERT_ID);
 		 		
 		$objInsert = db_insert($tableName, $options);
 		 		
 		$objInsert->fields($arrData);
 		
 		$lastID = $objInsert->execute();
 		
 		return($lastID);
 		
 	}
 	*/
 	
 	
 	/**
 	 * delete some row from the table
 	 */
 	/*
 	public function delete($tableName, $where){
 		
 		if(is_array($where) == false)
 			UniteFunctionsUG::throwError("The 'where' ver must be array");
 		
 		if(empty($where) == false)
 			UniteFunctionsUG::throwError("The 'where' array must be not empty");
 		
 		$options = array('return' => Database::RETURN_AFFECTED);
 		
 		$objDelete = db_delete($tableName, $options);
 		
 		foreach($where as $field => $value)
 			$objDelete->condition($field, $value, "="); 		
 		
 		$numRows = $objDelete->execute();
 		
 		return($numRows);
 	}
 	*/
	
}



?>