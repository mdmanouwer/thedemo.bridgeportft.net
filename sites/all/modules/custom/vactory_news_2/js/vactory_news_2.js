/**
 * @file
 * Js file for vactory_news_2.
 *
 */

(function ($) {
    $(document).ready(function () {

        var is_rtl = ($('html[dir="rtl"]').length) ? true : false;
        // Slick big slides slider
        $('.block-slider-large-vactory-news .slider-grid').each(function() {
            var $self = $(this);

            if ($self.children().length == 1) {
                $self.addClass('single-item');
            }
        });

        $('.block-slider-large-vactory-news .slider-grid').slick({
            dots: true,
            arrows: true,
            infinite: true,
            cssEase: 'cubic-bezier(0.585, -0.005, 0.635, 0.920)',
            useTransform: true,
            speed: 800,
            rtl: is_rtl,
            appendDots: $('.block-slider-large-vactory-news .view-content'),
            nextArrow: '<button type="button" class="slick-arrow next ripple-effect"><i class="icon-chevron-right"></i></button>',
            prevArrow: '<button type="button" class="slick-arrow prev ripple-effect"><i class="icon-chevron-left"></i></button>'
        });

        // Slick small slides slider
        $('.block-slider-small-vactory-news .slider-grid').each(function() {
            var $self = $(this);

            if ($self.children().length == 1) {
                $self.addClass('single-item');
            }
        });

        $('.block-slider-small-vactory-news .slider-grid').slick({
            dots: true,
            arrows: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            cssEase: 'cubic-bezier(0.585, -0.005, 0.635, 0.920)',
            useTransform: true,
            rtl: is_rtl,
            speed: 800,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            appendDots: $('.block-slider-small-vactory-news .view-content'),
            nextArrow: '<button type="button" class="slick-arrow next ripple-effect"><i class="icon-chevron-right"></i></button>',
            prevArrow: '<button type="button" class="slick-arrow prev ripple-effect"><i class="icon-chevron-left"></i></button>'
        });

        // Masonry
        $('.block-masonry-vactory-news .mansory-grid').masonry({
            // options
            itemSelector: '.mansory-grid-item'
        });

        // Tall small blocks.
        if ($.fn.masonry) {
            $('.article-card.use-background-thumbnail').each(function () {
                var $self = $(this),
                    $container = $self.find('.article-card__thumbnail > a'),
                    imgUrl = $container.find('img').prop('src');

                if (imgUrl) {
                    $container
                        .css('backgroundImage', 'url(' + imgUrl + ')');
                }
            });
        }

    });
})(jQuery);