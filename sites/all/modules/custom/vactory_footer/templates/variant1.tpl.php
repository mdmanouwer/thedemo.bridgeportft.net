<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<footer role="footer">
  <div class="vf-footer-variant1">
    <div class="vf-footer">
      <div class="vf-footer__layout">
        <div class="vf-footer__layout__primary-menu">
          <?php print $main_menu; ?>
        </div>
        <div class="vf-footer__layout__secondary-menu">
          <?php print $secondary_menu; ?>
        </div>
      </div>
    </div>
  </div>
</footer>