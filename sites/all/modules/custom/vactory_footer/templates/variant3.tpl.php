<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<footer role="footer">
  <div class="vf-footer-variant3">
    <div class="container">
      <div class="row bt_equal_height">
        <?php if (isset($col_1) && !empty($col_1)): ?>
          <div class="col-md-3 dotted">
            <?php print $col_1; ?>
          </div>
        <?php endif; ?>

        <?php if (isset($col_2) && !empty($col_2)): ?>
          <div class="col-md-3 dotted">
            <?php print $col_2; ?>
          </div>
        <?php endif; ?>

        <?php if (isset($col_3) && !empty($col_3)): ?>
          <div class="col-md-3 dotted">
            <?php print $col_3; ?>
          </div>
        <?php endif; ?>

        <?php if (isset($col_4) && !empty($col_4)): ?>
          <div class="col-md-3 last-col dotted">
            <?php print $col_4; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <?php if (isset($t3_bottom) && !empty($t3_bottom)): ?>
      <div class="vf-footer--bottom">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <?php print $t3_bottom; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

  </div>
</footer>
