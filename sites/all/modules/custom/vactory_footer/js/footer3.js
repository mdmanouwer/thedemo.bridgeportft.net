;
(function ($, viewport) {

    'use strict';

    var footer = {};
    footer.behaviors = {};
    footer.structure = {};
    footer.dom = {
        element: $('.vf-footer-variant3')
    };

    footer.init = function () {
        footer.behaviors.accordion();
    };

    footer.behaviors.accordion = function () {
        footer.dom.element.find('.expanded.heading > a').on('click', function (e) {
            e.preventDefault();
            $(this).next().slideToggle();
            $(this).toggleClass('open');
        });

        // Responsive stuff.
        // Going back from mobile to desktop should display links again
        // slideToggle, remember ?
        $(document).ready(function () {
            $(window).on('resize', function () {
                if (viewport.is('xs')) {
                    footer.dom.element.find('.expanded.heading > .menu-wrapper').hide();
                    footer.dom.element.find('.expanded.heading > a').removeClass('open');
                }
                else {
                    footer.dom.element.find('.expanded.heading > .menu-wrapper').show();
                    footer.dom.element.find('.expanded.heading > a').addClass('open');
                }
            });
        });

    };

    footer.init();

})(jQuery, ResponsiveBootstrapToolkit);
