;
(function ($) {

    'use strict';

    var header = {};
    header.behaviors = {};
    header.structure = {};
    header.components = {};
    header.dom = {
        'mobile_menu': $('#vhm-menu')
    };

    header.init = function () {
        header.behaviors.preprocess();
        header.structure.mobile_menu();
        header.components.dropdown();
        header.behaviors.submenu();
        header.behaviors.stick();
        header.behaviors.burger();
        header.behaviors.scrollIndicator();
    };

    header.behaviors.preprocess = function () {
        //$('#main-wrapper, #vh-header').addClass('vh-push-left');
    };

    // Open submenu.
    header.behaviors.submenu = function () {
        // Desktop.
        $('.vh-primary-menu > .block__content > .menu-wrapper > .menu > li.expanded > a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > .menu-wrapper').removeClass('open');
            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > a').removeClass('selected');

            if (submenu.css('visibility') == 'visible') {
                selected.removeClass('selected');
                submenu.removeClass('open');
            } else {
                selected.addClass('selected');
                submenu.addClass('open');
            }
        });

        // Mobile
        $('.vhm-menu li.expanded>a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            //if (submenu.css('display') == 'block') {
            if (submenu.hasClass('is-open')) {
                selected.removeClass('selected');
                //submenu.slideUp();
                submenu.removeClass('is-open');

                submenu.find('.expanded>a').removeClass('selected');
                //submenu.find('.menu-wrapper').slideUp();
                submenu.find('.menu-wrapper').removeClass('is-open');
            } else {
                selected.addClass('selected');
                //submenu.slideDown();
                submenu.addClass('is-open');
            }
        });

        // Back button.
        $('.vhm-menu .vh-primary-menu li.expanded').each(function () {
            var selected = $(this).clone();
            var submenu = $(this).children('.menu-wrapper').children('ul');

            selected.find('a').text(Drupal.t('Back'));
            selected.removeClass().addClass('back-link');
            submenu.prepend(selected);
            var parent = selected.parent('ul').parent('.menu-wrapper');

            selected.on('click', function (event) {
                event.preventDefault();

                // Timeout. To let user see what was selected before.
                setTimeout(function () {
                    $('.vhm-menu a.selected').removeClass('selected');
                }, 300);

                parent.removeClass('is-open');
            });

        });

        $('.vhm-menu li.expanded>a[class^="active"]').trigger('click');

        // Sub menu close button.
        var $button_close = $('<div class="vh-menu-button-close"><div class="container"><a href="#." class="button-close"><i class="icon-close-circle-flat"></i></a></div></div>');
        $('.vh-header .vh-primary-menu .block__content > .menu-wrapper > .menu > .expanded > .menu-wrapper').append($button_close);
        $('.vh-menu-button-close .button-close').on('click', function (e) {
            e.preventDefault();
            $('.vh-header .vh-primary-menu .block__content > .menu-wrapper > .menu > li.expanded > a[class*="selected"]').trigger('click');
        });
    };

    // Sticky header.
    header.behaviors.stick = function () {
        var $head = $('#vh-header');
        var $offset = '-600px';

        //if ($('body').hasClass('logged-in')) {
        //    $offset = '100px';
        //}

        $('body').waypoint(function (direction) {

            // Case 1: When scroll happen & menu open.
            if ($('.vh-header.variant1 .vh-menus').css('display') == 'none') {
                $head.removeClass('is-sticky');
                return;
            }

            // Behavior: When Scroll +/- happen.
            if (direction === 'down') {
                $head.addClass('is-sticky');
            }
            else if (direction === 'up') {
                $head.removeClass('is-sticky');
            }

        }, {offset: $offset});


        $('body').waypoint(function (direction) {

            // Case 1: When scroll happen & menu open.
            if ($('.vh-header.variant1 .vh-menus').css('display') == 'none') {
                $head.removeClass('is-sticky');
                return;
            }

            // Behavior: When Scroll +/- happen.
            if (direction === 'down') {
                $head.addClass('is-pre__sticky');
            }
            else if (direction === 'up') {
                $head.removeClass('is-pre__sticky');
            }

        }, {offset: '-200px'});

    };

    // Burger button.
    header.behaviors.burger = function () {
        $('#mmenu-btn').click(function () {
            if (!header.dom.mobile_menu.hasClass('is-open')) {
                header.dom.mobile_menu.addClass('is-open');
                $('#main-wrapper, #vh-header').addClass('is-open');
                $('#main-wrapper').addClass('has-overlay');
                $('html, body').addClass('vh-disable__scroll'); // Lock screen.
            }
            else {
                header.dom.mobile_menu.removeClass('is-open');
                $('#main-wrapper, #vh-header').removeClass('is-open');
                $('#main-wrapper').removeClass('has-overlay');
                $('html, body').removeClass('vh-disable__scroll'); // Unlock screen.
            }
        });

        $('#main-wrapper').click(function () {
            if ($(this).hasClass('has-overlay')) {
                $('#mmenu-btn').trigger('click');
            }
        });

        $('#mmenu-close-btn').click(function () {
            $('#mmenu-btn').trigger('click');
        });

    };

    // Scroll Indicator.
    header.behaviors.scrollIndicator = function () {
        var element = $('.vh-header .scroll-indicator');
        $(window).scroll(function () {
            var offsettop = parseInt($(this).scrollTop());
            var parentHeight = parseInt($('body, html').height() - $(window).height());
            var vscrollwidth = offsettop / parentHeight * 100;
            element.css({width: vscrollwidth + '%'});
        });
    };

    // Header dropdown.
    header.components.dropdown = function () {
        $('.vh-header select.cs-select').each(function (i, el) {
            var form = $(this).parents('form');
            new SelectFx(el, {
                onChange: function (val) {
                    form.submit();
                }
            });
        });
    };

    // Clone primary menu to mobile.
    header.structure.mobile_menu = function () {
        if (!$('.vh-header > .container > .block.vh-primary-menu').length) {
            return;
        }

        var $menu = $('.vh-header > .container > .block.vh-primary-menu').clone();
        $('#vhm-menu .vh-primary-menu').append($menu);
    };

    header.init();

})(jQuery);