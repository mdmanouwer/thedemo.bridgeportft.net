;
(function ($) {

    'use strict';

    var header = {};
    header.behaviors = {};
    header.structure = {};
    header.dom = {
        'mobile_menu': $('#vhm-menu')
    };

    header.init = function () {
        header.behaviors.preprocess();
        header.structure.mobile_menu();
        header.behaviors.submenu();
        header.behaviors.burger();
    };

    header.behaviors.preprocess = function () {
        //$('#main-wrapper, #vh-header').addClass('vh-push-left');
    };

    // Open submenu.
    header.behaviors.submenu = function () {
        // Desktop.
        $('.vh-primary-menu > .block__content > .menu-wrapper > .menu > li.expanded > a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > .menu-wrapper').removeClass('open');
            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > a').removeClass('selected');

            if (submenu.css('visibility') == 'visible') {
                selected.removeClass('selected');
                submenu.removeClass('open');
            } else {
                selected.addClass('selected');
                submenu.addClass('open');
            }
        });

        // Mobile
        $('.vhm-menu li.expanded>a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            if (submenu.css('display') == 'block') {
                selected.removeClass('selected');
                submenu.slideUp();

                submenu.find('.expanded>a').removeClass('selected');
                submenu.find('.menu-wrapper').slideUp();
            } else {
                selected.addClass('selected');
                submenu.slideDown();
            }
        });

        $('.vhm-menu li.expanded>a[class^="active"]').trigger('click');

        // Sub menu close button.
        var $button_close = $('<div class="vh-menu-button-close"><div class="container"><a href="#." class="button-close"><i class="icon-close-circle-flat"></i></a></div></div>');
        $('.vh-header .vh-primary-menu .block__content > .menu-wrapper > .menu > .expanded > .menu-wrapper').append($button_close);
        $('.vh-menu-button-close .button-close').on('click', function (e) {
            e.preventDefault();
            $('.vh-header .vh-primary-menu .block__content > .menu-wrapper > .menu > li.expanded > a[class*="selected"]').trigger('click');
        });
    };

    // Burger button.
    header.behaviors.burger = function () {
        $('#mmenu-btn').click(function () {
            if (!header.dom.mobile_menu.hasClass('is-open')) {
                header.dom.mobile_menu.addClass('is-open');
                $('#mmenu-btn').addClass('is-active');
                $('#main-wrapper, #vh-header').addClass('is-open');
                $('#main-wrapper').addClass('has-overlay');
            }
            else {
                header.dom.mobile_menu.removeClass('is-open');
                $('#mmenu-btn').removeClass('is-active');
                $('#main-wrapper, #vh-header').removeClass('is-open');
                $('#main-wrapper').removeClass('has-overlay');
            }
        });

        $('#main-wrapper').click(function () {
            if ($(this).hasClass('has-overlay')) {
                $('#mmenu-btn').trigger('click');
            }
        });
    };


    Drupal.behaviors.vHeader3 = {
        attach: function (context, settings) {
            var direction = (settings.v_header.hamburger_position == 1) ? 'vh3-push-left' : 'vh3-push-right';

            //if (settings.v_header.hamburger_position == 0) {
            //    header.dom.mobile_menu.addClass('is-leftside');
            //}

            $('#main-wrapper, #vh-header', context).addClass(direction);
        }
    };

    // Clone primary menu to mobile.
    header.structure.mobile_menu = function () {
        if (!$('.vh-header > .container > .block.vh-primary-menu').length) {
            return;
        }

        var $menu = $('.vh-header > .container > .block.vh-primary-menu').clone();
        $('#vhm-primary-menu-placeholder').replaceWith($menu);
    };

    header.init();

})(jQuery);