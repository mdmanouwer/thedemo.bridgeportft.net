<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="variant4">
  <div id="vh-header"
       class="vh-header vh-sticky vh-header-large large-menu__layer x-wide-menu__layer x-small-menu__layer">
    <div class="container">

      <!-- Burger menu -->
      <div class="vhm-menu__hamburger hidden vh-mobile__show">
        <button id="mmenu-btn" class="hamburger hamburger--spin"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
        </button>
      </div>
      <!-- ./Burger menu -->

      <!-- Logo -->
      <div class="block vh-logo">
        <div class="block__content">
          <?php print $defaults_blocks['logo']; ?>
        </div>
      </div>
      <!-- ./Logo -->

      <!-- Main Menu -->
      <div class="block vh-primary-menu">
        <div class="block__content">
          <?php print $defaults_blocks['main_menu']; ?>
        </div>
      </div>
      <!-- ./Main Menu -->

      <!-- Search Icon Button -->
      <div class="block vh-component__search">
        <div class="block__content">
          <?php print $defaults_blocks['search_icon']; ?>
        </div>
      </div>
      <!-- ./Search Icon Button -->

      <!-- Social Links -->
      <div class="block vh-social-links">
        <div class="block__content">
          <?php print $defaults_blocks['social_media']; ?>
        </div>
      </div>
      <!-- ./Social Links -->

      <!-- Language Dropdown -->
      <div class="block vh-language_dropdown">
        <div class="block__content">
          <?php print $defaults_blocks['language_dropdown']; ?>
        </div>
      </div>
      <!-- ./Language Dropdown -->

    </div> <!-- ./Container -->

    <div class="scroll-indicator"></div>

  </div> <!-- ./Header -->

  <div id="vhm-menu" class="vhm-menu <?php print ($config['is_rtl']) ? 'open-from-right' : 'open-from-left'; ?> narrow x-full">
    <div class="vhm-menu__top-wrapper">
      <div class="vhm-menu__logo">
        <?php print $defaults_blocks['logo']; ?>
      </div>
      <div class="vhm-menu__hamburger">
        <button id="mmenu-close-btn" class="hamburger hamburger--collapse is-active"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
        </button>
      </div>
    </div>
    <div class="vhm-menu_search">
      <?php print $defaults_blocks['search_form']; ?>
    </div>
    <div class="vh-primary-menu">
      <?php // Using Javascript. We clone $main_menu and append it here. ?>
    </div>
  </div>
</div>
