<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="variant2">
  <div id="vh-header" class="vh-header vh-sticky vh-header-large">
    <div class="container">
      <?php print $desktop; ?>

      <div class="vhm-menu__hamburger hidden vh-mobile__show">
        <button id="mmenu-btn" class="hamburger hamburger--spin"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
        </button>
      </div>
    </div>
  </div>

  <div id="vhm-menu" class="vhm-menu">
    <?php print $mobile; ?>
  </div>
</div>
