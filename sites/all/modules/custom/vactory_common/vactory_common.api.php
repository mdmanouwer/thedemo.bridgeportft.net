<?php

/**
 * Defines default values for attaching Cover Field to content types
 */
function MYMODULE_vactory_common_cover_field(){
  // Example of attaching Field Cover automatically to News content type in
  // the second poistion.
  return array(
    'bundle' => 'news',
    'weight' => 2,
    // Optional:
    'widget' => 'image', // default value : 'media'
    'field_group' => 'group_name'
   );
}