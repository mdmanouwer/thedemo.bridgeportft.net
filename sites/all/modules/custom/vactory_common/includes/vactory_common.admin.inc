<?php

/**
 * @file
 * Admin menu callbacks of the module.
 */

/**
 * Callback to output table of content types.
 */
function vactory_common_cover_field_table() {
  global $base_url;
  $header = array(t('Content type'), t('Machine name'), t('Widget'), t('Operations'));
  $content_types = node_type_get_types();
  $rows[] = array();
  foreach ($content_types as $content_type) {
    $links = NULL;
    $widget = '-';
    $field_instance = field_info_instance('node', VACTORY_COMMON_FIELD_COVER, $content_type->type);
    if (!$field_instance) {
      $links = l('Add field (Image Widget)', 'aadmin/config/content/cover-field/' . $content_type->type . '/add/image') . ' ' .
      l('Add field (Media Widget)', 'admin/config/content/cover-field/' . $content_type->type . '/add/media');
    }
    else {
      $links = l('Delete field', 'admin/config/content/cover-field/' . $content_type->type . '/delete/null');
      $widget = ucfirst($field_instance['widget']['module']);
    }

    $rows[] = array(
      $content_type->name,
      $content_type->type,
      $widget,
      $links,
    );
  }
  $build['types'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => isset($rows) ? $rows : array(),
    '#empty' => t('No content types available.'),
  );
  return $build;
}

/**
 * Callback to manage Field Cover in a specified content type.
 *
 * @param string $bundle
 *    Content type machine name.
 * @param string $action
 *    Action string (add or delete).
 */
function vactory_common_cover_field_edit($bundle, $action, $widget = NULL) {
  $field_instance = field_info_instance('node', VACTORY_COMMON_FIELD_COVER, $bundle);
  if ($action == 'add' && !$field_instance) {
    module_load_include('inc', 'vactory_common', 'includes/vactory_common.fields');
    $field_cover_instance_settings = vactory_common_field_cover_instance_settings($bundle, $widget);
    field_create_instance($field_cover_instance_settings);
    drupal_set_message(t('Field attached successfully to the content type.'));
  }
  elseif ($action == 'delete' && $field_instance) {
    field_delete_instance($field_instance);
    drupal_set_message(t('Field successfully deleted from the content type.'));
  }
  drupal_goto('admin/config/content/cover-field');
}
