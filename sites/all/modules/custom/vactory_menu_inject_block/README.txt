Usage
-----
1. Add a new menu item or edit an existing one.
2. Set the path to "<block>"
3. Choose the block to be injected from the "Block to inject" drop-down.
4. Save menu item
