<?php
/**
 * @file
 * vactory_newsletter_mailchimp.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function vactory_newsletter_mailchimp_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['mailchimp_signup-newsletter'] = array(
    'cache' => 2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'newsletter',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'mailchimp_signup',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'radix' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'radix',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'starter1' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'starter1',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
