<?php

/**
 * @file
 * Theming functions of vactory_skeleton module.
 */

/**
 * Implements hook_theme().
 */
function vactory_skeleton_theme(&$existing, $type, $theme, $path) {
  return array(
    'vactory_skeleton_blocks_table' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Theme a drag-to-reorder table of blocks selection checkboxes.
 */
function theme_vactory_skeleton_blocks_table($variables) {
  $form = $variables['form'];
  $region = $form['#region'];
  drupal_add_tabledrag('vactory-skeleton-blocks' . $region, 'order', 'sibling', 'block-weight');
  $table['attributes']['id'] = 'vactory-skeleton-blocks' . $region;
  $table['header'] = array(
    t('Blocks'),
    t('Enabled'),
    t('Weight'),
  );

  // Generate table of draggable menu names.
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title_display'])) {
      $block = &$form[$key];
      $row = array();
      $row[] = drupal_render($block['title_display']);
      $row[] = drupal_render($block['enabled']);
      $block['weight']['#attributes']['class'] = array('block-weight');
      $row[] = drupal_render($block['weight']);
      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }
  $table['rows'] = $rows;

  return theme('table', $table);
}
