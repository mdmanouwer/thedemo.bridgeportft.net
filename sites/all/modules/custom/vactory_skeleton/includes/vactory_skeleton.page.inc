<?php

/**
 * @file
 * Page build functions of vactory_skeleton module.
 */

/**
 * Implements hook_page_build().
 */
function vactory_skeleton_page_build(&$page) {
  // Global $vactory_skeleton_regions;.
  foreach (_vactory_skeleton_get_enabled_regions() as $region => $region_value) {
    $enabled_blocks = variable_get('vactory_skeleton_enabled_blocks_' . $region);
    if (isset($enabled_blocks)) {
      $blocks = array();
      foreach ($enabled_blocks as $key => $value) {
        $block = block_load($value['module'], $value['delta']);
        $block_render = _block_render_blocks(array($block));
        $block_renderable_array = _block_get_renderable_array($block_render);
        $output = drupal_render($block_renderable_array);
        $blocks[$value['bid']] = array('#markup' => $output);
      }
      if (isset($page[$region])) {
        $page[$region] = array_merge($page[$region], $blocks);
      }
      else {
        $page[$region] = $blocks;
      }
    }
  }
}
