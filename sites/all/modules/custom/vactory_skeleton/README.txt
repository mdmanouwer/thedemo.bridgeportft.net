

Vactory Skeleton 1.x for Drupal 7.x
--------------------------
Vactory Skeleton allows you to manage/build your page regions
content from available Drupal block.


Installation
------------
Vactory Skeleton can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it (and its requirement,
CTools, Blockify) on the `admin/modules` page.

The Blockify module is also enabled and configured to exposes a number of core Drupal elements as blocks.
(Logo, Site name, Site slogan, Page title, Breadcrumb ,Messages)

The Vactory Skeleton provides an administration settings page on : admin/config/vactory/vactory_skeleton


Configuration
-------

1. Go to the administration settings page : admin/config/vactory/vactory_skeleton
2. Under each Region tab (Top, Header, Bridge, Footer, Bottom),
   you can enable/disable different Drupal Blocks to show in each region.
2. The blocks can be re-ordered using the draggable table.
3. Save the configuration.


Maintainers
-----------

- Taoufiq El hattami
