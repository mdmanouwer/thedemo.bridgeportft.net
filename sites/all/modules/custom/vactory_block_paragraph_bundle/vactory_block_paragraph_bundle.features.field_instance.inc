<?php
/**
 * @file
 * vactory_block_paragraph_bundle.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function vactory_block_paragraph_bundle_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-paragraph_block-field_block'.
  $field_instances['paragraphs_item-paragraph_block-field_block'] = array(
    'bundle' => 'paragraph_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'blockreference',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'show_empty_blocks' => FALSE,
        ),
        'type' => 'blockreference_default',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_block',
    'label' => 'Block',
    'required' => 1,
    'settings' => array(
      'blockreference_modules' => array(
        'backup_migrate' => 0,
        'block' => 0,
        'blockgroup' => 0,
        'blockify' => 0,
        'comment' => 0,
        'context_ui' => 0,
        'current_search' => 0,
        'devel' => 0,
        'entityform_block' => 0,
        'facetapi' => 0,
        'lang_dropdown' => 0,
        'locale' => 0,
        'menu' => 0,
        'node' => 0,
        'nodeblock' => 0,
        'search' => 0,
        'shortcut' => 0,
        'social_media_links' => 0,
        'system' => 0,
        'user' => 0,
        'vactory_footer' => 0,
        'vactory_header' => 0,
        'vactory_search_overlay' => 0,
        'views' => 0,
        'webform' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'paragraphs_item-paragraph_block-field_css'.
  $field_instances['paragraphs_item-paragraph_block-field_css'] = array(
    'bundle' => 'paragraph_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_css',
    'label' => 'CSS',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'vactory_paragraphs_css',
      'settings' => array(),
      'type' => 'vactory_paragraphs_css_text_widget',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'paragraphs_item-paragraph_block-field_p_body'.
  $field_instances['paragraphs_item-paragraph_block-field_p_body'] = array(
    'bundle' => 'paragraph_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_p_body',
    'label' => 'Content',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'paragraphs_item-paragraph_block-field_p_title'.
  $field_instances['paragraphs_item-paragraph_block-field_p_title'] = array(
    'bundle' => 'paragraph_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'h2',
    'field_name' => 'field_p_title',
    'label' => 'Title',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Block');
  t('CSS');
  t('Content');
  t('Title');

  return $field_instances;
}
