<?php

/**
 * @file
 * Custom theme hooks.
 */

function thedemo_theme($existing, $type, $theme, $path) {
  $items = array();

  $items['user_login'] = array(
    'render element'       => 'form',
    'path'                 => $path . '/templates/user',
    'template'             => 'user-login',
    'preprocess functions' => array(
      'vactory_preprocess_user_login'
    ),
  );
  $items['user_register_form'] = array(
    'render element'       => 'form',
    'path'                 => $path . '/templates/user',
    'template'             => 'user-register-form',
    'preprocess functions' => array(
      'vactory_preprocess_user_register_form'
    ),
  );
  $items['user_pass'] = array(
    'render element'       => 'form',
    'path'                 => $path . '/templates/user',
    'template'             => 'user-pass',
    'preprocess functions' => array(
      'vactory_preprocess_user_pass'
    ),
  );
  return $items;
}

/**
 * Implements hook_process_html().
 */
function thedemo_process_html(&$vars) {
  $vars['footer_scripts'] = drupal_get_js('header'); // Move header scripts to footer.
  $vars['header_scripts'] = drupal_get_js('header_scripts'); // Scope for required scripts like jQuery (Case: Other inline scripts may call jQuery before its execution).
}

/**
 * Implements hook_js_alter().
 */
function thedemo_js_alter(&$javascript) {
  // Move jQuery to "header_scripts" scope.
  $jq_version = variable_get('jquery_update_jquery_version', '1.10');
  if (isset($javascript['sites/all/modules/contrib/jquery_update/replace/jquery/' . $jq_version . '/jquery.min.js'])) {
    $javascript['sites/all/modules/contrib/jquery_update/replace/jquery/' . $jq_version . '/jquery.min.js']['scope'] = 'header_scripts';
  }

  if (isset($javascript['misc/jquery.once.js'])) {
    $javascript['misc/jquery.once.js']['scope'] = 'header_scripts';
  }
}
