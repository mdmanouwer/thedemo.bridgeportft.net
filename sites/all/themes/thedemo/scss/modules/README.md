# Theme modules override.

Ce dossier permet de surcharger le comportement SASS des modules custom/contrib.

Le dossier **modules** permet de surcharger une fonctionnalité d'un module.
Le dossier **variables** permet de surcharger les variables que fournis un module.