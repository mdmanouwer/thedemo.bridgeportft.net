##### 07. INPUT FIELDS / DROP DOWNS

    @example
    <div class="row">
      <div class="col-md-6">
        <div class="col-md-8">
              <form class="webform-client-form">
                  <div>
                    <div class="form-item webform-component webform-component-textfield webform-component--votre-nom">
                        <input required="required" placeholder="Olivirer Delas" class="form-control form-text required" type="text" id="edit-submitted-votre-nom" name="submitted[votre_nom]" value="" size="60" maxlength="128" aria-required="true">
                    </div>
                    <div class="email-wrapper form-item webform-component webform-component-email webform-component--votre-adresse-email">
                        <input required="required" class="email email-input form-control form-text form-email required" placeholder="exemple@yahoo.fr" type="email" id="edit-submitted-votre-adresse-email" name="submitted[votre_adresse_email]" size="60" aria-required="true">
                    </div>
                    <div class="email-wrapper form-item webform-component webform-component-textfield webform-component--telephone">
                        <input required="required" placeholder="06 - XX - XX - XX - XX" class="email-input form-control form-text required" type="text" id="edit-submitted-telephone" name="submitted[telephone]" value="" size="60" maxlength="128" aria-required="true">
                    </div>
                  </div>
              </form>
        </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-10">
            <select class="selectpicker">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
            </select>
        </div>
      </div>
      </div>
      <div class="row ptm pbm">
        <div class="col-md-6">
          <select class="" multiple="multiple">
            <option value="1">Formations &amp; Emplois</option>
            <option value="2">Manifestations &amp; Spectacles</option>
            <option value="3">Autre</option>
            </select>
        </div>
      </div>
    </div>
