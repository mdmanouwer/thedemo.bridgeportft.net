##### SERVICES

	@example
	<div class="services">
    <div class="col-md-3 box-content">
      <article>
        <a href="#">
          <div class="intro">
            <div class="img">
              <img src="http://placehold.it/86x90&text=image">
            </div>
            <div class="text">
              <p>Consulter mes mandats</p>
            </div>
          </div>
          <div class="desc">
            <div class="text">
              <h2>Consulter mes mandats</h2>
              <p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius</p>
            </div>
            <div class="link">
              <span class="read-service">Découvrir le service ⇨</span>
            </div>
          </div>
        </a>
      </article>
    </div>
    <div class="col-md-3 box-content">
      <article>
        <a href="#">
          <div class="intro">
            <div class="img">
              <img src="http://placehold.it/86x90&text=image">
            </div>
            <div class="text">
              <p>E-dpo</p>
            </div>
          </div>
          <div class="desc">
            <div class="text">
              <h2>E-dpo</h2>
              <p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius</p>
            </div>
            <div class="link">
              <span class="read-service">Découvrir le service ⇨</span>
            </div>
          </div>
        </a>
      </article>
    </div>
    <div class="col-md-3 box-content">
      <article>
        <a href="#">
          <div class="intro">
            <div class="img">
              <img src="http://placehold.it/86x90&text=image">
            </div>
            <div class="text">
              <p>Déclarer une oeuvre</p>
            </div>
          </div>
          <div class="desc">
            <div class="text">
              <h2>Déclarer une oeuvre</h2>
              <p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius</p>
            </div>
            <div class="link">
              <span class="read-service">Découvrir le service ⇨</span>
            </div>
          </div>
        </a>
      </article>
    </div>
    <div class="col-md-3 box-content">
      <article>
        <a href="#">
          <div class="intro">
            <div class="img">
              <img src="http://placehold.it/86x90&text=image">
            </div>
            <div class="text">
              <p>E-dpo</p>
            </div>
          </div>
          <div class="desc">
            <div class="text">
              <h2>E-dpo</h2>
              <p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius</p>
            </div>
            <div class="link">
              <span class="read-service">Découvrir le service ⇨</span>
            </div>
          </div>
        </a>
      </article>
    </div>
	</div>
