##### TABLE

	@example
	<table>
    <thead>
      <tr>
        <th></th>
        <th>TF1</th>
        <th>F2</th>
        <th>F3</th>
        <th>Bouquet C+</th>
        <th>M6</th>
        <th>F5</th>
        <th>Arte</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">Tarif coef 100 (1)</th>
        <td>331,75€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
      </tr>
      <tr>
        <th scope="row">Tarif coef 150 (2)</th>
        <td>331,75€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
        <td>375,30€</td>
      </tr>
    </tbody>
	</table>
