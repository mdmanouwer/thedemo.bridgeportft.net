##### TEMPLATES ARTICLES

    @example
    <div id="myCarousel" class="sg-caroussel carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <article class="node node-news node-teaser clearfix" about="/fr/content/adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
            <img src="http://placehold.it/979x381&text=image">
          </article>
        </div>
        <div class="item">
          <article class="node node-news node-teaser clearfix" about="/fr/content/adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
            <img src="http://placehold.it/979x381&text=image">
          </article>
        </div>
        <div class="item">
          <article class="node node-news node-teaser clearfix" about="/fr/content/adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
            <img src="http://placehold.it/979x381&text=image">
          </article>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="btn-slider-left btn-label icon-arrow-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="btn-slider-right btn-label icon-arrow-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
