##### Contenu Annexe

    @example
    <div class="contenu-annexe">
      <div class="text">
        <p>Mauris non tempor quam, et lacinia sapien. Mauris accumsan eros eget libero posuere vulputate. </p>
      </div>
      <a href="#" class="more-link pull-right"><i class="icon-arrow-right"> </i> Découvrir</a>
    </div>
