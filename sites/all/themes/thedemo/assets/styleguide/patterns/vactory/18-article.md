##### TEMPLATES ARTICLES

    @example
    <div class="row">
      <div class="b-listing--items  news-col-3">
        <div class="views-row views-row-odd views-row-first col-md-4 col-sm-4 col-xs-12">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/191" class="img-link-wrapper">
                  <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/me" typeof="skos:Concept" property="rdfs:label skos:prefLabel">me</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/abigo-te">Abigo Te</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row views-row-even col-md-4 col-sm-4 col-xs-12">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/191" class="img-link-wrapper" class="img-link-wrapper">
                  <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/slislemut" typeof="skos:Concept" property="rdfs:label skos:prefLabel">slislemut</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/abigo-te">Abigo Te</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row views-row-odd views-row-last col-md-4 col-sm-4 col-xs-12">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/155" class="img-link-wrapper">
                <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/st" typeof="skos:Concept" property="rdfs:label skos:prefLabel">st</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-05-29T23:30:30+02:00">29 May 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/luptatum-pagus-patria-vicis">Luptatum Pagus Patria Vicis</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/155">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="b-listing--items  news-col-2">
        <div class="views-row views-row-odd views-row-first col-md-6">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-half">
                <div class="img-wr">
                  <a href="/fr/node/191" class="img-link-wrapper"><img src="http://placehold.it/580x176&text=image"></a>
                </div>
                <div class="tags-date">
                  <span class="btn btn-tag"><a href="/fr/news-tags/me" typeof="skos:Concept" property="rdfs:label skos:prefLabel">me</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span>
                </div>
                <div class="text">
                  <div class="views-field-title h2-large"><a href="/en/content/abigo-te">Abigo Te</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna...</p></div>
                  <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row-even col-md-6">
          <div class="views-row">
            <div class="views-field views-field-nothing">
              <div class="field-content">
                <div class="news-item news-quarter">
                  <div class="img-wr">
                    <a href="/fr/node/191" class="img-link-wrapper"><img src="http://placehold.it/325x170&text=image"></a>
                  </div>
                  <div class="tags-date">
                    <span class="btn btn-tag"><a href="/fr/news-tags/slislemut" typeof="skos:Concept" property="rdfs:label skos:prefLabel">slislemut</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span>
                  </div>
                  <div class="text">
                    <div class="views-field-title h2-x-small"><a href="/en/content/abigo-te">Abigo Te</a></div>
                    <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum...</p></div>
                    <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="views-row">
            <div class="views-field views-field-nothing">
              <div class="field-content">
                <div class="news-item news-quarter">
                  <div class="img-wr">
                    <a href="/fr/node/155" class="img-link-wrapper"> <img src="http://placehold.it/325x170&text=image"> </a>
                  </div>
                  <div class="tags-date">
                    <span class="btn btn-tag"><a href="/fr/news-tags/st" typeof="skos:Concept" property="rdfs:label skos:prefLabel">st</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-05-29T23:30:30+02:00">29 May 2016</span></span>
                  </div>
                  <div class="text">
                    <div class="views-field-title h2-x-small"><a href="/en/content/luptatum-pagus-patria-vicis">Luptatum Pagus Patria Vicis</a></div>
                    <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum...</p></div>
                    <div class="read-article"><a href="/fr/node/155">→ Lire l’article</a></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="b-listing--items  news-row-2">
        <div class="views-row views-row-odd views-row-first col-md-12">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-full">
                <a href="/fr/node/191" class="img-link-wrapper">
                  <img src="http://placehold.it/1160x300&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/me" typeof="skos:Concept" property="rdfs:label skos:prefLabel">me</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-large"><a href="/en/content/abigo-te">Abigo Te</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius. Aliquam augue humo nimis secundum vero totoAppellatio luctus...</p></div>
                  <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row views-row-even col-md-4 col-sm-4">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/191" class="img-link-wrapper">
                  <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/slislemut" typeof="skos:Concept" property="rdfs:label skos:prefLabel">slislemut</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/abigo-te">Abigo Te</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/191">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row views-row-odd views-row-last col-md-4 col-sm-4">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/155" class="img-link-wrapper">
                <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/st" typeof="skos:Concept" property="rdfs:label skos:prefLabel">st</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-05-29T23:30:30+02:00">29 May 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/luptatum-pagus-patria-vicis">Luptatum Pagus Patria Vicis</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/155">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="views-row views-row-odd views-row-last col-md-4 col-sm-4">
          <div class="views-field views-field-nothing">
            <div class="field-content">
              <div class="news-item news-third">
                <a href="/fr/node/155" class="img-link-wrapper">
                <img src="http://placehold.it/325x150&text=image">
                </a>
                <div><span class="btn btn-tag"><a href="/fr/news-tags/st" typeof="skos:Concept" property="rdfs:label skos:prefLabel">st</a></span> <span class="x-small views-field-field-date"><span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-05-29T23:30:30+02:00">29 May 2016</span></span></div>
                <div class="text">
                  <div class="views-field-title h2-x-small"><a href="/en/content/luptatum-pagus-patria-vicis">Luptatum Pagus Patria Vicis</a></div>
                  <div><p>totoAppellatio luctus molior vicis. Eligo enim jumentum lucidus magna populus probo proprius...</p></div>
                  <div class="read-article"><a href="/fr/node/155">→ Lire l’article</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

##### Ticker
    @example
    <div class="views news-ticker">
      <div class="views-field views-field-nothing">
        <div class="field-content">
          <div class="news-item news-quarter">
            <div class="img-wr">
              <a href="/fr/node/191">
                <img src="http://placehold.it/240x135&amp;text=image">
              </a>
            </div>
            <div class="tags-date">
              <span class="btn btn-tag">
                <a href="/fr/news-tags/slislemut" typeof="skos:Concept" property="rdfs:label skos:prefLabel">slislemut</a>
              </span>
              <span class="x-small views-field-field-date">
                <span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2016-07-01T00:00:00+02:00">1 July 2016</span>
              </span>
            </div>
            <div class="text">
              <div class="views-field-title h2-x-small">
                <a href="/en/content/abigo-te"> Lutter contre les délocalisations et améliorer la diffusion du cinema : 2 objectifs prioritaires pour le CNC </a>
              </div>
              <div>
                <p>La SACD accueillait le 18 mai sur son stand Xavier Lardoux, directeur du cinéma du CNC, qui en a profité pour faire un tour d'actualité des réformes engagées et à venir pour le cinéma.</p>
              </div>
              <a href="#" class="btn-close-ticker btn">
                <i class="icon-cross"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
