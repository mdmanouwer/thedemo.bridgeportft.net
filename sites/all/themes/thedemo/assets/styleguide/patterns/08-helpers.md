##### 07. Tags / Dates

    @example
    <div class="row">
    <span class="sg-d-block">Tag field</span>
      <div class="item-tags">
        <div class="field field-name-field-vactory-news-theme field-type-taxonomy-term-reference field-label-hidden">
          <div class="field-items">
            <div class="field-item even">Nouveautés</div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <span class="sg-d-block">Date field</span>
      <div class="item-date">
        <div class="field field-name-field-vactory-date field-type-datetime field-label-hidden">
          <div class="field-items">
            <div class="field-item even">
              <span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2017-04-21T00:00:00+02:00">04/21/2017</span>
            </div>
          </div>
        </div>
      </div>
    </div>
