##### 09.Pagination

    @example
    <div class="row">
      <div class="col-md-12">
        <div class="item-list">
          <ul class="pagination">
            <li class="pager-first first">
              <a href="#">&lt;&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-previous">
              <a href="#">&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-item">
              <a href="#">1</a>
            </li>
            <li class="pager-current active">
              <span>2</span>
            </li>
            <li class="pager-item">
              <a href="#">3</a>
            </li>
            <li class="pager-next">
              <a href="#">&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
            <li class="pager-last last">
              <a href="#">&gt;&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
          </ul>
        </div>
        <div class="item-list">
          <ul class="pagination text-left">
            <li class="pager-first first">
              <a href="#">&lt;&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-previous">
              <a href="#">&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-item">
              <a href="#">1</a>
            </li>
            <li class="pager-current active">
              <span>2</span>
            </li>
            <li class="pager-item">
              <a href="#">3</a>
            </li>
            <li class="pager-next">
              <a href="#">&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
            <li class="pager-last last">
              <a href="#">&gt;&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
          </ul>
        </div>
        <div class="item-list">
          <ul class="pagination text-right">
            <li class="pager-first first">
              <a href="#">&lt;&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-previous">
              <a href="#">&lt;
                <!-- <i class="icon-arrow_left_circle"></i> -->
              </a>
            </li>
            <li class="pager-item">
              <a href="#">1</a>
            </li>
            <li class="pager-current active">
              <span>2</span>
            </li>
            <li class="pager-item">
              <a href="#">3</a>
            </li>
            <li class="pager-next">
              <a href="#">&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
            <li class="pager-last last">
              <a href="#">&gt;&gt;
                <!-- <i class="icon-arrow_right_circle"></i> -->
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="pager-wrapper">
      <div class="item-list">
       <ul class="pager">
          <li class="pager-first first"><a href="#">&lt;&lt;</a></li>
          <li class="pager-previous"><a href="#">&lt;</a></li>
          <li class="pager-item"><a title="Aller à la page 1" href="#">1</a></li>
          <li class="pager-item"><a title="Aller à la page 2" href="#">2</a></li>
          <li class="pager-current">3</li>
          <li class="pager-item"><a title="Aller à la page 4" href="#">4</a></li>
          <li class="pager-next"><a href="#">&gt;</a></li>
          <li class="pager-last last"><a href="#">&gt;&gt;</a></li>
       </ul>
      </div>
    </div>
    </div>
