##### 01.Couleurs

	@example
	<div class="sg-color">
    <div class="sg-color-item brand-primary"><span>#d3222a</span></div>
	<div class="sg-color-item brand-secondary"><span>#00693e</span></div>
    <div class="sg-color-item gray-base"><span>#000000</span></div>
    <div class="sg-color-item gray-darker"><span>#1C1310</span></div>
    <div class="sg-color-item gray-dark"><span>#434343</span></div>
    <div class="sg-color-item gray"><span>#4A4A4A</span></div>
    <div class="sg-color-item gray-light"><span>#F4F4F4</span></div>
    <div class="sg-color-item gray-lighter"><span>#FFFFFF</span></div>
	</div>

